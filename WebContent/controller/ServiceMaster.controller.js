sap.ui.define([
	"sap/ui/core/mvc/Controller", 
	"sap/ui/core/routing/History",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter",
	"sap/ui/model/Sorter",
	"./Formatter"
], function(Controller, History, FilterOperator, Filter, Sorter, Formatter){
	"use strict";
	var ServiceMasterController = Controller.extend("SeidorB1UI5.controller.ServiceMaster", {
		
		formatter: Formatter,
			
		onInit: function() {
			if(document.cookie.length === 0){
				this.handleLogout();
			} else {
				//GUARDA ELA FUNCION ACTUALIZAR PARA FORZAR POSTERIOR CARGA DE PANTALLA
				var oPrevious = History.getInstance().getPreviousHash();
				const onAfterShow = () => this.handleActualizar();
				this._afterShowDelegate = { onAfterShow };
				if(oPrevious !== "ServiceCreate" || oPrevious !== "Page" || oPrevious !== "ServiceCard")
					this.getView().addEventDelegate(this._afterShowDelegate);
				
			
				if(!oServiceJSONData || oServiceJSONData === undefined){
					sap.ui.core.BusyIndicator.show(0);
					oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
					getService();
					oServiceJSONData = JSON.parse(sessionStorage.getItem("services"));
				}
				
				if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
					oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
				} else if(!sessionStorage.getItem("tiles")){
					this.handleLogout();
				}
				
				var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
				this.byId("avatar").setInitials(iAvatar);
				this.byId("avatar").setTooltip("Usuario: " + avatar);
				
				var oTable = this.getView().byId("tablaServices");
				var oModelTable = new sap.ui.model.json.JSONModel();
		
				oModelTable.setData(oServiceJSONData);
				oTable.setModel(oModelTable);
		    	this.getView().setModel(oModelTable);
		    	
		    	oTable.sort(this.getView().byId("CreationDate"), sap.ui.table.SortOrder.Descending, true);
		    	oTable.sort(this.getView().byId("ServiceCallID"), sap.ui.table.SortOrder.Descending, true);
		    	this.getView().setModel(this.oModelList);
		    	
		    	var fnPress = this.handleSelect.bind(this);
		    	this.modes=[{
		    		key: "Multi",
					text: "Multiple Actions",
					handler: function(){
						var oTemplate = new sap.ui.table.RowAction({items: [
							new sap.ui.table.RowActionItem({
								type: "Navigation",
								press: fnPress,
								visible: "{Available}"
							})
						]});
						return [2, oTemplate];
					}
		    	}];
		    	this.getView().setModel(new sap.ui.model.json.JSONModel({items: this.modes}), "modes");
				this.switchState("Multi");
			}
		},
		
		handleLogout: function (evt) {
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},
		
		switchState : function (sKey) {
			var oTable = this.byId("tablaServices");
			var iCount = 0;
			var oTemplate = oTable.getRowActionTemplate();
			if (oTemplate) {
				oTemplate.destroy();
				oTemplate = null;
			}
	
			for (var i = 0; i < this.modes.length; i++) {
				if (sKey == this.modes[i].key) {
					var aRes = this.modes[i].handler();
					iCount = aRes[0];
					oTemplate = aRes[1];
					break;
				}
			}
	
			oTable.setRowActionTemplate(oTemplate);
			oTable.setRowActionCount(iCount);
		},
		
		handleActualizar: function(){
			oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
			getService();
			oServiceJSONData = JSON.parse(sessionStorage.getItem("services"));
			
			var oTable = this.getView().byId("tablaServices");
		    var data = oTable.getModel();
			var oData = data.getData();
			
			data.setData(oServiceJSONData);
			data.refresh();
			this.getView().setModel(data);
		},
		
		handleSelect: function(oEvent) {
			var oRow = oEvent.getParameter("row");
			var oItem = oEvent.getParameter("item");
			ordersTable = oItem;
			selectedOrder = oEvent.getParameter("row").getCells()[0].getText();
			sap.ui.core.UIComponent.getRouterFor(this).navTo("ServiceDetail",{from: "servicemaster", contextPath: oItem.getBindingContext().getPath().substr(7)});
		},
		
		handleCrearBtnPress: function() {
			sap.ui.core.UIComponent.getRouterFor(this).navTo("ServiceCreate",{});
		},
		
		handleCrearCard: function(){
			sap.ui.core.UIComponent.getRouterFor(this).navTo("ServiceCard",{});
		},
		
		_filter : function () {
			var oFilter = null;
	
			if (this._oGlobalFilter && this._oPriceFilter) {
				oFilter = new Filter([this._oGlobalFilter, this._oPriceFilter], true);
			} else if (this._oGlobalFilter) {
				oFilter = this._oGlobalFilter;
			} else if (this._oPriceFilter) {
				oFilter = this._oPriceFilter;
			}
	
			// actualizar la lista de enlace	
			var list = this.getView().byId("list");
			var binding = list.getBinding("items");
			binding.filter(oFilter);		
		},
	
		filterGlobally : function(oEvent) {
			var sQuery = oEvent.getParameter("query");
			this._oGlobalFilter = null;
	
			if (sQuery) {
				this._oGlobalFilter = new Filter([
					new Filter("CustomerName", FilterOperator.Contains, sQuery),
					new Filter("CreationDate", FilterOperator.Contains, sQuery.toString()),
					new Filter("U_SEI_ESTADO", FilterOperator.Contains, sQuery),
					new Filter("ServiceCallID", FilterOperator.Contains, sQuery.toString())
				], false);
			}
	
			this._filter();
		},
		
		onBack : function () {
			var sPreviousHash = History.getInstance().getPreviousHash();
	
			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				// There is no history!
				// replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("Page", null, true);
			}
		},
		
		handleTraeEqC : function(){
            getAsync("tarjetasEquipo");
            var oTarjetas = JSON.parse(sessionStorage.getItem("tarjetasEquipo"));
            
            const items = oTarjetas.value;
            const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
            const header = Object.keys(items[0]);
            const csv = [
              header.join(';'), // header row first
              ...items.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(';'))
            ].join('\r\n');
            
            var blob = new Blob([csv], {type: 'text/csv'});
            if(window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(blob, filename);
            }
            else{
                var elem = window.document.createElement('a');
                elem.href = window.URL.createObjectURL(blob);
                elem.download = 'TarjetasEquipo.csv';        
                document.body.appendChild(elem);
                elem.click();        
                document.body.removeChild(elem);
            }
            
        }
	});
	return ServiceMasterController;
}, true);