sap.ui.define([
	"jquery.sap.global",
	"sap/m/MessageToast",
	"./Formatter",
	"sap/ui/core/Fragment",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter",	
	"sap/ui/model/json/JSONModel",
	'sap/ui/core/library',
	"sap/m/MessageBox",
	"./Formatter"
	
], function(jQuery, MessageToast, Fragment, Formatter, Controller, FilterOperator, Filter, JSONModel, coreLibrary, MessageBox) {
	"use strict";
	
	var ValueState = coreLibrary.ValueState;

	var ReemplazoCreateController = Controller.extend("SeidorB1UI5.controller.ReemplazoCreate", {
		/*
		 * Declaracion de variables
		 */
		mensaje: null, oTable: null, oJSONModel: null, formatter: Formatter,
		codigo: null, descripcion: null, fecha1: null, fecha2: null, inpReemplazo: false,		
		oModelSel: null, oSelectD2: null, oSelectD3: null, busy: new sap.m.BusyDialog(),
		
		onInit: function() {
			var view = this.getView(), oModel = null;
			this.estado = null;
			
			
			if(strAccion === "modificar"){
				sap.ui.core.UIComponent.getRouterFor(this).attachRouteMatched(function(oEvent) {
		    		oModel = new sap.ui.model.json.JSONModel();
		    		oModel.setData(oReemplazoJSONData);
		        	view.setModel(oModel);
					
					if (oEvent.getParameter("name") === "ReemplazoCreate") {
						if(ordersTable){				
							var oListItem = ordersTable;			    
							var context = new sap.ui.model.Context(view.getModel(), oListItem.getBindingContext().getPath());
							this.path = oListItem.getBindingContext().getPath().substr(7);
							view.setBindingContext(context);
							
							if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
								oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
							} else if(!sessionStorage.getItem("tiles")){
								this.handleLogout();
							}
							
							var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
							this.byId("avatar").setInitials(iAvatar);
							this.byId("avatar").setTooltip("Usuario: " + avatar);
							
							// CARGA DE REEMPLAZO YA EXISTENTE
							this.cargaReemplazo();
						} else {
						 	this.handleNavButton();
						}
				    }
				}, this);
			} else if (strAccion === "crear")	{
			
				if(document.cookie.length === 0){
					this.handleLogout();
				} else {
					if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
						oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
					} else if(!sessionStorage.getItem("tiles")){
						this.handleLogout();
					}
					
					var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
					this.byId("avatar").setInitials(iAvatar);
					this.byId("avatar").setTooltip("Usuario: " + avatar);
					
					this.getView().byId("inpColegio").setValue(oDatosUsuario.des_suc);
					this.getView().byId("inpColegioReemplazar").setValue(oDatosUsuario.des_suc);
					
					this.getBancos(); this.getAfps(); this.getIsapres();
					
					this.oJSONModel = new JSONModel();//new sap.ui.model.json.JSONModel();
					var oModel = new JSONModel();
					oModel.setData(oReemplazoJSONData);
					this.oJSONModel = oModel;
					
					this.getView().setModel(this.oJSONModel);
					
				}
			} else {
				this.handleNavButton();
			}
				
		},
		
		cargaReemplazo: function(){
			// TODO: Recorre el item seleccionado y carga la pestana A Reemplazar
			this.byId("inpRutSearch").setValue("");
			this.byId("inpNombreReemplazar").setValue("");
			this.byId("inpTipoFuncionario").setValue("");
			this.byId("inpDescripcionCargo").setValue("");
			
			var oModel = oModel = new sap.ui.model.json.JSONModel();
			var oTable = this.getView().byId("tblHorasReemplazar");
			var oColumn = this.getView().byId("colListHorasReemplazar");
			
			var oItemsTbl = this.getView().byId("tblHorasReemplazante");
			var oColListItem = this.getView().byId("colListHorasReemplazante");
			
			
			this.getView().byId("chckLicen").setSelected(false);
			this.getView().byId("dpFechaDesdeVigente").setValue("");
			this.getView().byId("dpFechaHastaVigente").setValue("");

			// validar si se modifica para bloquear o no el campo
			this.getView().byId("RB1-1").setEnabled(false);
			this.getView().byId("RB1-2").setEnabled(false);
			this.getView().byId("dpFechaDesdeNueva").setEnabled(false);
			this.getView().byId("dpFechaHastaNueva").setEnabled(false);
			this.getView().byId("dpFechaDesdeNueva").setValue("");
			this.getView().byId("dpFechaHastaNueva").setValue("");
			
			oModel.setData(oReemplazoJSONData);
			oTable.setModel(oModel);
			oTable.bindItems("/HContratadas", oColumn);
			
			oItemsTbl.setModel(oModel);
			oItemsTbl.bindItems("/HContratadas", oColListItem);
			
			this.getView().getModel().refresh();
			
			
			// TODO: Recorre el item seleccionado y carga la pestana Reemplazante
			
			//this.byId("inpNombreReemplazante").setValue("");
			//this.byId("inpColegioReemplazar").setValue("");
			//this.byId("inpTipoFuncionarioReemplazante").setValue("");
			//this.byId("inpDescripcionCargoReemplazante").setValue("");
			this.byId("dpNacimiento").setValue("");
			this.byId("cbxEstadoCivil").setValue("");
			this.byId("cbxTipoCuenta").setValue("");
			this.byId("cbxBanco").setValue("");

			this.byId("dpInicioReemplazo").setValue("");
			this.byId("dpFinReemplazo").setValue("");
			this.byId("inpNCuenta").setValue("");
			
			this.byId("inpDireccion").setValue("");
			this.byId("inpTelefono").setValue("");
			this.byId("inpCorreo").setValue("");
			this.byId("cbxAFP").setValue("");
			this.byId("cbxIsapre").setValue("");
		},
		
		getCabecera: function(_strRut){
			select="", filtro ="";
			select="", filtro="Rut eq '" + _strRut + "'";
			getAsync("ReemplCab");
			oReemplazoJSONData.Cabecera = JSON.parse(sessionStorage.getItem("ReemplCab")).value;
			
			if(oReemplazoJSONData.Cabecera.length > 0){
				this.getView().byId("inpTipoFuncionario").setValue(oReemplazoJSONData.Cabecera.DesCargo);
				this.getView().byId("inpDescripcionCargo").setValue(oReemplazoJSONData.Cabecera.DesFuncionario);
				
				this.getView().byId("inpTipoFuncionarioReemplazante").setValue(oReemplazoJSONData.Cabecera.DesCargo);
				this.getView().byId("inpDescripcionCargoReemplazante").setValue(oReemplazoJSONData.Cabecera.DesFuncionario);
			}
			
		},
		
		getHContratadas: function(_strRut){
			select="", filtro ="";
			select="", filtro="Rut eq '" + _strRut + "'";
			getAsync("ReemplHCon");
			oReemplazoJSONData.HContratadas = JSON.parse(sessionStorage.getItem("ReemplHCon")).value;
			
			var oModel = oModel = new sap.ui.model.json.JSONModel();
			var oTable = this.getView().byId("tblHorasReemplazar");
			var oColumn = this.getView().byId("colListHorasReemplazar");
			
			var oItemsTbl = this.getView().byId("tblHorasReemplazante");
			var oColListItem = this.getView().byId("colListHorasReemplazante");
			
			oModel.setData(oReemplazoJSONData);
			oTable.setModel(oModel);
			oTable.bindItems("/HContratadas", oColumn);
			
			oItemsTbl.setModel(oModel);
			oItemsTbl.bindItems("/HContratadas", oColListItem);
			
			this.getView().getModel().refresh();
		},
		
		handleChange: function(oEvent) {
			this.fecha1 = oEvent.getParameter("value");
			
			var fecIni = "", fecFin = "", estado = "", SEI_UNIDAD = "", SEI_ESTADO = "", SEI_BETWEN = "";
			
			fecIni = this.getView().byId("dpFechaDesdeSolicitudes").getValue();
			fecFin = this.getView().byId("dpFechaHastaSolicitudes").getValue();
			
			if(fecIni != "" && fecFin != ""){
				if(fecIni <= fecFin){
					// TODO: validar fecha sea 2 dias antes a la fecha actual
					// si cumple, copiar fecha en otra pestana, sino borrar las fechas ingresadas
					// y mandar mensaje indicando el error en el ingreso de la fecha
					
					this.getView().byId("dpInicioReemplazo").setValue(fecIni);
					this.getView().byId("dpFinReemplazo").setValue(fecFin);
					
				}else{
					new sap.m.MessageBox.show(
							"Las fechas de inicio no puede ser mayor a la fin", { 
		               	icon: sap.m.MessageBox.Icon.ERROR, 
		              	title: "Error en Busqueda de Solicitudes de Compra."
		            });
				}
			}
		},
		
		getLicencias: function(_strRut){
			select="", filtro="";
			select="", filtro="Rut eq '" + _strRut + "'";
			getAsync("ReemplLice");
			oReemplazoJSONData.Licencias = JSON.parse(sessionStorage.getItem("ReemplLice")).value;
			
			if(oReemplazoJSONData.Licencias.length == 0){
				this.getView().byId("chckLicen").setSelected(false);
				
				this.getView().byId("RB1-1").setEnabled(true);
				this.getView().byId("RB1-2").setEnabled(true);
				
				this.getView().byId("dpFechaDesdeNueva").setEnabled(true);
				this.getView().byId("dpFechaHastaNueva").setEnabled(true);
			} else {
				this.getView().byId("dpFechaDesdeVigente").setValue(oReemplazoJSONData.Licencias.FechaDesde);
				this.getView().byId("dpFechaHastaVigente").setValue(oReemplazoJSONData.Licencias.FechaHasta);
				
				this.getView().byId("dpInicioReemplazo").setValue(oReemplazoJSONData.Licencias.FechaDesde);
				this.getView().byId("dpFinReemplazo").setValue(oReemplazoJSONData.Licencias.FechaHasta);
			}
		},
		
		getBancos: function(){
			getAsync("ReemplBancos");
			oReemplazoJSONData.Bancos = JSON.parse(sessionStorage.getItem("ReemplBancos")).value;
		},
		
		getAfps: function(){
			getAsync("ReemplAFP")
			oReemplazoJSONData.Afps = JSON.parse(sessionStorage.getItem("ReemplAFP")).value;
		},
		
		getIsapres: function(){
			getAsync("ReemplIsapre");
			oReemplazoJSONData.Isapres = JSON.parse(sessionStorage.getItem("ReemplIsapre")).value;
		},
		
		getReemplazante: function(_strRut){
			select="", filtro ="";
			select="U_idcodigo,U_idfecnac,U_idestciv,U_rptipocta,U_rpcodban,U_rpctacte,U_dodirecc,U_donumtel,U_idemail,U_previ,U_salud", filtro="U_idcodigo eq '" + _strRut + "'";
			getAsync("Reemplazante");
			oReemplazoJSONData.Reemplazante = JSON.parse(sessionStorage.getItem("Reemplazante")).value;
			
			if(oReemplazoJSONData.Reemplazante.length > 0){
				let dato = oReemplazoJSONData.Reemplazante[0];
				//this.byId("inpNombreReemplazante").setValue("");
				//this.byId("inpColegioReemplazar").setValue("");
				//this.byId("inpTipoFuncionarioReemplazante").setValue("");
				//this.byId("inpDescripcionCargoReemplazante").setValue("");
				//this.byId("dpInicioReemplazo").setValue("");
				//this.byId("dpFinReemplazo").setValue("");
				
				this.byId("dpNacimiento").setValue(dato.U_idfecnac);
				this.byId("cbxEstadoCivil").setValue(this.setFormater('EstadoCivil', dato.U_idestciv));
				this.byId("cbxTipoCuenta").setValue(this.setFormater('TipoCta', dato.U_rptipocta));
				
				this.byId("cbxBanco").setValue(this.setFormater('Banco', dato.U_rpcodban));
				
				//this.byId("cbxTReem").setValue(dato);
				this.byId("inpNCuenta").setValue(dato.U_rpctacte);
				
				this.byId("inpDireccion").setValue(dato.U_dodirecc);
				this.byId("inpTelefono").setValue(dato.U_donumtel);
				this.byId("inpCorreo").setValue(dato.U_idemail);
				this.byId("cbxAFP").setValue(this.setFormater('AFP', dato.U_previ));
				this.byId("cbxIsapre").setValue(this.setFormater('Isapre', dato.U_salud));
			}
		},
		
		setFormater: function(_tipoFor, _dato){
			let resultado;
			switch(_tipoFor) {
				case 'EstadoCivil':
					switch(_dato){
						case 'S':
							resultado = 'Solter@';
							break;
						case 'C':
							resultado = 'Casad@';
							break;
						case 'D':
							resultado = 'Divorciad@';
							break;
						case 'V':
							resultado = 'Viud@';
							break;
					}
					break;
				case 'TipoCta':
					switch(_dato){
						case '-':
							resultado = 'Sin Valor';
							break;
						case '1':
							resultado = 'Corriente';
							break;
						case '2':
							resultado = 'Ahorro';
							break;
						case '3':
							resultado = 'Vista/Otras';
							break;
					}
					break;
					
				case 'Banco':
					oReemplazoJSONData.Bancos.find(function(dato, index){
						if(dato.BankCode == _dato){
							resultado = dato.BankName;
						}
					});
					break;
				case 'AFP':
					oReemplazoJSONData.Afps.find(function(dato, index){
						if(dato.U_codigo == _dato){
							resultado = dato.U_nombre;
						}
					});
					break;
				case 'Isapre':
					oReemplazoJSONData.Isapres.find(function(dato, index){
						if(dato.U_codigo == _dato){
							resultado = dato.U_nombre;
						}
					});
					break;
			}
			return resultado
		},
		
		handleCrearBtn: function(){
			//TODO Limpia los formularios
			//this.limpiaCampos();
			//TODO Muestra campos de busqueda
			this.byId("lblRutSearchReemplazanteSet").setVisible(true); 
			this.byId("inpRutSearchReemplazanteSet").setVisible(true);
			this.byId("btnBuscUsu").setVisible(true);
			//TODO Oculta campos de busqueda
			this.byId("lblRutSearchReemplazante").setVisible(false);
			this.byId("inpRutSearchReemplazante").setVisible(false);
			this.byId("btnCreaUsu").setVisible(false);
			//TODO Habilita campos para creacion
			this.byId("inpNombreReemplazante").setEnabled(true);
			this.byId("dpNacimiento").setEnabled(true);
			this.byId("cbxTReem").setEnabled(true);
			this.byId("inpNCuenta").setEnabled(true);
			this.byId("inpDireccion").setEnabled(true);
			this.byId("inpTelefono").setEnabled(true);
			this.byId("inpCorreo").setEnabled(true);
		},
		
		handleBuscarBtn: function(){
			//TODO Limpia los formularios
			this.limpiaReemplazante(false);
			//TODO Muestra campos de busqueda
			this.byId("lblRutSearchReemplazante").setVisible(true);
			this.byId("inpRutSearchReemplazante").setVisible(true);
			this.byId("btnCreaUsu").setVisible(true);
			//TODO Oculta campos de busqueda
			this.byId("lblRutSearchReemplazanteSet").setVisible(false); 
			this.byId("inpRutSearchReemplazanteSet").setVisible(false);
			this.byId("btnBuscUsu").setVisible(false);
			//TODO Deshabilita campos para busqueda
			this.byId("inpNombreReemplazante").setEnabled(false);
			this.byId("dpNacimiento").setEnabled(false);
			this.byId("cbxTReem").setEnabled(false);
			this.byId("inpNCuenta").setEnabled(false);
			this.byId("inpDireccion").setEnabled(false);
			this.byId("inpTelefono").setEnabled(false);
			this.byId("inpCorreo").setEnabled(false);
		},
		
		limpiaReemplazar: function(oEvent){
			this.byId("inpRutSearch").setValue("");
			this.byId("inpNombreReemplazar").setValue("");
			this.byId("inpTipoFuncionario").setValue("");
			this.byId("inpDescripcionCargo").setValue("");
			
			this.byId('tblHorasReemplazar').setModel(new sap.ui.model.json.JSONModel({data: []}));
			this.byId('tblHorasReemplazante').setModel(new sap.ui.model.json.JSONModel({data: []}));
			
			this.getView().byId("chckLicen").setSelected(false);
			this.getView().byId("dpFechaDesdeVigente").setValue("");
			this.getView().byId("dpFechaHastaVigente").setValue("");
			
			this.getView().byId("RB1-1").setEnabled(false);
			this.getView().byId("RB1-2").setEnabled(false);
			this.getView().byId("dpFechaDesdeNueva").setEnabled(false);
			this.getView().byId("dpFechaHastaNueva").setEnabled(false);
			this.getView().byId("dpFechaDesdeNueva").setValue("");
			this.getView().byId("dpFechaHastaNueva").setValue("");
		},
		
		limpiaReemplazante: function(oEvent){
			this.byId("inpNombreReemplazante").setValue("");
			this.byId("dpNacimiento").setValue("");
			this.byId("cbxEstadoCivil").setValue("");
			this.byId("cbxTipoCuenta").setValue("");
			this.byId("cbxBanco").setValue("");
			
			if(oEvent === true){
				//this.byId("inpColegioReemplazar").setValue("");
				this.byId("inpTipoFuncionarioReemplazante").setValue("");
				this.byId("inpDescripcionCargoReemplazante").setValue("");
				this.byId("dpInicioReemplazo").setValue("");
				this.byId("dpFinReemplazo").setValue("");
			}
			
			this.byId("cbxTReem").setValue("");
			this.byId("inpNCuenta").setValue("");
			
			this.byId("inpDireccion").setValue("");
			this.byId("inpTelefono").setValue("");
			this.byId("inpCorreo").setValue("");
			this.byId("cbxAFP").setValue("");
			this.byId("cbxIsapre").setValue("");
		},
		
		handleLogout: function (evt) {
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},
		
		handleNavButton: function(oEvent) {
			if (this._oDialog)
				this.cierraFragment();
			
			var sPreviousHash = sap.ui.core.routing.History.getInstance().getPreviousHash();
			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				// There is no history!
				// replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("ReemplazoMaster", oItems, true);
			}
		},
		
		onValueHelpRequestedNS: function() {
			this.inpReemplazo = true;
			this._oInput = this.getView().byId("inpRutSearch");//inpCliente
			this.oColModel = new sap.ui.model.json.JSONModel();
			this.oColModel.setData({"cols": [{ "label": "Rut", "template": "U_idcodigo", "width": "10rem" }, { "label": "Nombre", "template": "U_idapenom" }]});
			
			var aCols = this.oColModel.getData().cols;
			this._oBasicSearchField = new sap.m.SearchField({showSearchButton: false});
			
			this._oValueHelpDialog = sap.ui.xmlfragment("SeidorB1UI5.view.fragment.Empleado", this);
			this.getView().addDependent(this._oValueHelpDialog);

			this._oValueHelpDialog.getFilterBar().setBasicSearch(this._oBasicSearchField);
			
			var oToken = new sap.m.Token();
			oToken.setKey(this._oInput.getSelectedKey());
			oToken.setText(this._oInput.getValue());
			this._oValueHelpDialog.setTokens([oToken]);
			this._oValueHelpDialog.open();
		},

		onFilterBarSearch: function (oEvent) {
			this.busy.open();
			var sSearchQuery = this._oBasicSearchField.getValue(),
				aSelectionSet = oEvent.getParameter("selectionSet"), 
				cerrar = this.busy.close(),
				sLike = "", 
				estado = true,
				mensaje = "";
			
			var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
				if (oControl.getValue()) {
					aResult.push(new sap.ui.model.Filter({
						path: oControl.getName(),
						operator: sap.ui.model.FilterOperator.Contains,
						value1: oControl.getValue()
					}));
				}

				return aResult;
			}, []);
			
			if(sSearchQuery.length >= 1 && sSearchQuery.length <= 2){
				estado = false;
				mensaje = "El Nombre del Socio de Negocion debe tener una cantidad de caracteres mayor o igual a 3. "
			} 
			
			if(aFilters.length != 0){
				if(aFilters[0].oValue1.length >= 1 && aFilters[0].oValue1.length <= 2) {
					estado = false;
					mensaje = mensaje + "El Codigo del Socio de Negocion debe tener una cantidad de caracteres mayor o igual a 3"
				}
			}
			
			if(estado) {
				sSearchQuery = sSearchQuery.toUpperCase();
				sLike = (sSearchQuery.isNullOrEmpty()) ? "" : " and contains(U_idprinom, '" + sSearchQuery + "')";
				sLike = (aFilters.length == 0) ? sLike + "" : (aFilters[0].oValue1.isNullOrEmpty()) ? sLike + "" : sLike + " and contains(U_idcodigo, '" + aFilters[0].oValue1 + "')";
				
				if(!sLike.isNullOrEmpty()){
					sessionStorage.removeItem("CodigoEmpleado");
					
					select = ""; limit = ""; filtro = "";
					limit = "";select = "";filtro = "U_usdim1 eq '" + oDatosUsuario.cod_suc + "'" + sLike;
					getAsync('CodigoEmpleado');
									
					//this.oCampos.BusinessPartners = JSON.parse(sessionStorage.getItem("CodigoEmpleado")).value;
					//this.oJSONModel = JSON.parse(sessionStorage.getItem("CodigoEmpleado")).value;
					this.oJSONModel.setData(JSON.parse(sessionStorage.getItem("CodigoEmpleado")).value);
					this._oValueHelpDialog.getTableAsync().then(function (oTable) {
						var oModel = new sap.ui.model.json.JSONModel();
						oTable.setModel(oModel);
						
						oTable.setModel(this.oJSONModel);
						oTable.setModel(this.oColModel, "columns");
	
						if (oTable.bindRows) {
							oTable.bindAggregation("rows", "/");
						}
	
						if (oTable.bindItems) {
							oTable.bindAggregation("items", "/", function () {
								return new sap.m.ColumnListItem({
									cells: aCols.map(function (column) {
										return new sap.m.Label({ text: "{" + column.template + "}" });
									})
								});
							});
						}
						
						this._oValueHelpDialog.update();
					}.bind(this));
					
					cerrar;
				} else {
					new sap.m.MessageBox.show(
							"Se debe Ingresar datos para la busqueda.",{
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "Error en el ingreso de los datos.",
							onClose: function(oAction) {
								cerrar;
							}
				  	    });
				}
			} else {
				new sap.m.MessageBox.show(
						mensaje,{
						icon: sap.m.MessageBox.Icon.ERROR,
						title: "Error en el ingreso de los datos.",
						onClose: function(oAction) {
							cerrar;
						}
			  	    });
			}
		},
		
		/**
		 * Funcion de Evento Seleccion de Linea de la tabla. que carga los valores en los campos
		 * Cliente - Nombre y llama a los acuerdos globales asociados al socio de negocio seleccionado
		 * para llenar los datos en combobox Acuedo globales.
		 */
		onValueHelpOkPress: function (oEvent) {
			sap.ui.core.BusyIndicator.show(0);
			
			if(this.inpReemplazo){
			
				var aTokens = oEvent.getParameter("tokens");
				this._oInput.setValue(aTokens[0].getKey());
				var nom = aTokens[0].getText();
				nom = nom.replace(" ("+ aTokens[0].getKey() +")", "");
				
				this.byId("inpNombreReemplazar").setValue(nom);
				this.byId("inpRutSearch").setValue(aTokens[0].getKey());
				
				this.getCabecera(aTokens[0].getKey()); 
				this.getHContratadas(aTokens[0].getKey()); 
				this.getLicencias(aTokens[0].getKey());
			} else if(!this.inpReemplazo){
				var aTokens = oEvent.getParameter("tokens");
				this._oInput.setValue(aTokens[0].getKey());
				var nom = aTokens[0].getText();
				nom = nom.replace(" ("+ aTokens[0].getKey() +")", "");
				
				this.byId("inpNombreReemplazante").setValue(nom);
				this.byId("inpRutSearchReemplazante").setValue(aTokens[0].getKey());
				
				this.getReemplazante(aTokens[0].getKey());
			}
			
			var oModel = new JSONModel();
			oModel.setData(oReemplazoJSONData);
			this.oJSONModel = oModel;
			
			this.getView().setModel(this.oJSONModel);
			
			sap.ui.core.BusyIndicator.hide();
			this._oValueHelpDialog.close();
		},

		/**
		 * Funcion de Evento Boton Cancelar. el que llama a la funcion para el cierre 
		 * de la ventana oValueHelpDialog.
		 */
		onValueHelpCancelPress: function () {
			this._oValueHelpDialog.close();
		},

		onValueHelpAfterClose: function () {
			this._oValueHelpDialog.destroy();
		},
		
		onValueHelpRequestedRe: function() {
			this.inpReemplazo = false;
			this._oInput = this.getView().byId("inpRutSearchReemplazante");//inpCliente
			this.oColModel = new sap.ui.model.json.JSONModel();
			this.oColModel.setData({"cols": [{ "label": "Rut", "template": "U_idcodigo", "width": "10rem" }, { "label": "Nombre", "template": "U_idapenom" }]});
			
			var aCols = this.oColModel.getData().cols;
			this._oBasicSearchField = new sap.m.SearchField({showSearchButton: false});
			
			this._oValueHelpDialog = sap.ui.xmlfragment("SeidorB1UI5.view.fragment.Empleado", this);
			this.getView().addDependent(this._oValueHelpDialog);

			this._oValueHelpDialog.getFilterBar().setBasicSearch(this._oBasicSearchField);
			
			var oToken = new sap.m.Token();
			oToken.setKey(this._oInput.getSelectedKey());
			oToken.setText(this._oInput.getValue());
			this._oValueHelpDialog.setTokens([oToken]);
			this._oValueHelpDialog.open();
		},

		onFilterBarSearch: function (oEvent) {
			this.busy.open();
			var sSearchQuery = this._oBasicSearchField.getValue(),
				aSelectionSet = oEvent.getParameter("selectionSet"), 
				cerrar = this.busy.close(),
				sLike = "", 
				estado = true,
				mensaje = "";
			
			var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
				if (oControl.getValue()) {
					aResult.push(new sap.ui.model.Filter({
						path: oControl.getName(),
						operator: sap.ui.model.FilterOperator.Contains,
						value1: oControl.getValue()
					}));
				}

				return aResult;
			}, []);
			
			if(sSearchQuery.length >= 1 && sSearchQuery.length <= 2){
				estado = false;
				mensaje = "El Nombre del Funcionario debe tener una cantidad de caracteres mayor o igual a 3. "
			} 
			
			if(aFilters.length != 0){
				if(aFilters[0].oValue1.length >= 1 && aFilters[0].oValue1.length <= 2) {
					estado = false;
					mensaje = mensaje + "El Codigo del Funcionario debe tener una cantidad de caracteres mayor o igual a 3"
				}
			}
			
			if(estado) {
				sSearchQuery = sSearchQuery.toUpperCase();
				sLike = (sSearchQuery.isNullOrEmpty()) ? "" : " and contains(U_idprinom, '" + sSearchQuery + "')";
				sLike = (aFilters.length == 0) ? sLike + "" : (aFilters[0].oValue1.isNullOrEmpty()) ? sLike + "" : sLike + " and contains(U_idcodigo, '" + aFilters[0].oValue1 + "')";
				
				if(!sLike.isNullOrEmpty()){
					sessionStorage.removeItem("CodigoEmpleado");
					
					select = ""; limit = ""; filtro = "";
					limit = "";select = "";filtro = "U_usdim1 eq '" + oDatosUsuario.cod_suc + "'" + sLike;
					getAsync('CodigoEmpleado');
									
					//this.oCampos.BusinessPartners = JSON.parse(sessionStorage.getItem("CodigoEmpleado")).value;
					//this.oJSONModel = JSON.parse(sessionStorage.getItem("CodigoEmpleado")).value;
					this.oJSONModel.setData(JSON.parse(sessionStorage.getItem("CodigoEmpleado")).value);
					this._oValueHelpDialog.getTableAsync().then(function (oTable) {
						var oModel = new sap.ui.model.json.JSONModel();
						oTable.setModel(oModel);
						
						oTable.setModel(this.oJSONModel);
						oTable.setModel(this.oColModel, "columns");
	
						if (oTable.bindRows) {
							oTable.bindAggregation("rows", "/");
						}
	
						if (oTable.bindItems) {
							oTable.bindAggregation("items", "/", function () {
								return new sap.m.ColumnListItem({
									cells: aCols.map(function (column) {
										return new sap.m.Label({ text: "{" + column.template + "}" });
									})
								});
							});
						}
						
						this._oValueHelpDialog.update();
					}.bind(this));
					
					cerrar;
				} else {
					new sap.m.MessageBox.show(
							"Se debe Ingresar datos para la busqueda.",{
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "Error en el ingreso de los datos.",
							onClose: function(oAction) {
								cerrar;
							}
				  	    });
				}
			} else {
				new sap.m.MessageBox.show(
						mensaje,{
						icon: sap.m.MessageBox.Icon.ERROR,
						title: "Error en el ingreso de los datos.",
						onClose: function(oAction) {
							cerrar;
						}
			  	    });
			}
		},

		handleIcon: function(oEvent){
			this.pstValue = oEvent.getParameters().key;
			var src = oEvent.getSource();
		},
		
		handleChangeCb: function(oEvent){
			var oValidatedComboBox = oEvent.getSource(),
			sSelectedKey = oValidatedComboBox.getSelectedKey(),
			sValue = oValidatedComboBox.getValue();

			if (!sSelectedKey && sValue) {
				oValidatedComboBox.setValueState(ValueState.Error);
				oValidatedComboBox.setValueStateText("Por favor ingresa valor valido!");
			} else {
				oValidatedComboBox.setValueState(ValueState.None);
			}
		},
		
		handleSiguienteBtn: function(oEvent){
			this.byId("iconTabBar").setSelectedKey("2");
		},
		
		handleCancelBtn: function(oEvent) {
			var oTable = this.getView().byId("idItemsTable");
			if(oTable.getModel().getData().newItems !== undefined){
				var items = oTable.getItems();
				this.onDelete(items);
				this.limpiarCampos();
			} else {
				new MessageBox.show("No hay lineas creadas a eliminar", {
					icon: MessageBox.Icon.WARNING,
					title: "Informacion Crear Solicitud de Orden",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		handleAnteriorBtn: function(){
			this.byId("iconTabBar").setSelectedKey("1")
		},		
				
		handleAceptBtn: function(oEvent) {
			sap.ui.core.BusyIndicator.show(0);			
			this.validacionEnvio();
			
			//TODO: Mapear la informacion con la UDO
			
			if(this.mensaje === null) {
				var reemplazo = {DocumentLines:[]};
				var reemplazoItems = {};
				var items = this.getView().byId("idItemsTable").getItems();
				
				reemplazo.Requester 	    = JSON.parse(sessionStorage.getItem("tiles")).user_code;
				reemplazo.RequesterName 	= JSON.parse(sessionStorage.getItem("tiles")).user_name;
				reemplazo.RequriedDate 	= this.fecha1;				
				reemplazo.RequesterEmail 	= JSON.parse(sessionStorage.getItem("tiles")).user_email;
				reemplazo.U_SEI_UNIDAD		= JSON.parse(sessionStorage.getItem("tiles")).des_suc;
				reemplazo.U_SEI_VIRTUAL  	= 'tYES';
				reemplazo.U_SEI_TIPO_FINANCIAMIENTO = this.byId("cbxTipoFinanciamiento").getValue();
				reemplazo.U_SEI_DEPARTAMENTO = this.byId("cbxDepartamento").getValue();
				
				reemplazo.RequesterName 	= JSON.parse(sessionStorage.getItem("tiles")).user_name;
				reemplazo.RequriedDate 	= this.fecha1;				
				reemplazo.RequesterEmail 	= JSON.parse(sessionStorage.getItem("tiles")).user_email;
				reemplazo.U_SEI_UNIDAD 	= JSON.parse(sessionStorage.getItem("tiles")).des_suc;
				reemplazo.U_SEI_VIRTUAL  	= 'tNO';
				reemplazo.U_SEI_TIPO_FINANCIAMIENTO = this.byId("cbxTipoFinanciamiento").getValue();
				reemplazo.U_SEI_DEPARTAMENTO = this.byId("cbxDepartamento").getValue();
										
				for(var i = 0; i < items.length; i++){
					
					if(items[i].getCells()[0].getText() === "tYES"){					
						reemplazoItems = {
								ItemCode: 			items[i].getCells()[1].getText(),							
								ItemDescription:	items[i].getCells()[2].getText(),
								FreeText:			items[i].getCells()[3].getValue(),
								Quantity:			items[i].getCells()[4].getValue()
						}
						reemplazo.DocumentLines.push(reemplazoItems);
					}
				}
				
				var res = 0;
				if(reemplazo.DocumentLines.length > 0){
					res = setReemplazo(reemplazo);
					if(res > 0)
						this.onDelete(items);
						this.limpiarCampos();
				}
				
			} else {
				sap.m.MessageBox.show(this.mensaje, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
//LOGICA PARA SUBIR ARCHIVOS
		
		handleTypeMissmatch: function(oEvent) {
			var oItem = oEvent.getParameter("item");
			this.byId("UploadSet").removeIncompleteItem(oItem);
			
			var aFileTypes = oEvent.getSource().getProperty("fileTypes");
			aFileTypes.map(function(sType) {
				return "*." + sType;
			});
			MessageToast.show("El tipo de archivo " + oItem.getProperty("mediaType") +
									" no es el correcto. Solo se aceptan : " +
									aFileTypes.join(", "));
		},

		handleValueChange: function(oEvent) {
//			MessageToast.show("Press 'Upload File' to upload file '" +
//									oEvent.getParameter("newValue") + "'");
//			timeOut = oEvent.getParameter("files").length * 4000;
			this.byId("UploadSet").getIncompleteItems().forEach(function (oItem) {
				oItem.setUploadState("Complete");
			});
			
			this.byId("UploadSet")._oFileUploader.mProperties.value = "HOLA";
		},
		
		handleFilenameLengthExceed: function(oEvent) {
			var oItem = oEvent.getParameter("item");
			this.byId("UploadSet").removeIncompleteItem(oItem);
		    
			var aMaxFileLength = oEvent.getSource().getProperty("maxFileNameLength");
			MessageToast.show("El nombre de los archivos no puede superar los "+ aMaxFileLength +" caracteres de largo.");
			return false;
		},

		handleFileSizeExceed: function(oEvent) {
			var oItem = oEvent.getParameter("item");
			this.byId("UploadSet").removeIncompleteItem(oItem);
			
			var aMaxFileSize = oEvent.getSource().getProperty("maximumFileSize");
			MessageToast.show("El tamaño del archivo no puede superar los " + aMaxFileSize + "MB.");
		},
		
		fileToBase64: function (pFile, tStamp, fName){
			var oFile = {};
			var reader = new FileReader();
			
			reader.readAsDataURL(pFile);
			
			oFile.FileExt = fName.substring(fName.length -3);
		    oFile.FileName = tStamp + "_"+ fName.substring(0,fName.length -4);
			oFile.Data = "";
			
			reader.onload = function () {
				var base64 = reader.result.split(";");
				var b64 = base64[1].split(',')[1];
				oFile.Data = b64;
			};
			   
			reader.onerror = function (error) {
			    console.log('Error: ', error);
			};
			console.log("fileToBase64. " + oFile.Data);
			return oFile;
		},
		
		onUploadSelectedButton: function (oEvent) {
			var oUploadSet = this.byId("UploadSet");
			var tStamp = Date.now();
			var oFiles = [];
			var listAttach = [];
			var status = true;
			var oView = this;
			this.timeOut = oUploadSet.getIncompleteItems().length * 4000;
			
			oUploadSet.getIncompleteItems().forEach(function (oItem) {
				//if (oItem.getListItem().getSelected()) {
					oFiles.push(oView.fileToBase64(oItem.getFileObject(),tStamp, oItem.getProperty("fileName")));
				//}
			});
			
			oFiles.forEach(function(oFile){
				//console.log(oFile);
				setTimeout(() => {
					if(oFile.Data.length === 0){
						sap.m.MessageBox.show("No se pudo leer correctamente el archivo: " + oFile.FileName +", intentelo nuevamente", {
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "Informacion Crear el Reemplazo",
							onClose: function(oAction) {					
					    		sap.ui.core.BusyIndicator.hide();
							}
						});
						status = false;
					}else{
						//subir archivo a B1if
						var res = uploadAttachment(oFile);
						if(res.DIresult == "success"){
							oFile.Attachment = res.DImsg;
							listAttach.push({
					            "SourcePath": res.DImsg.substring(0,res.DImsg.search(oFile.FileName)-1),
					            "FileName": oFile.FileName,
					            "FileExtension": oFile.FileExt,
					            "Override": "Y",
					            "CopyToTargetDoc": "Y"
					        });
						}
						else{
							oFile.Attachment = "";
							status = false;
						}
					}
				},4000);
			});
			
			//body pa setear attch al documento
			setTimeout(() => {
				this.listAttach = listAttach;
			},oView.timeOut);
			
			return status;
		},
		
		onExit: function() {
			if (this._oDialog) {
//				this._oDialog.destroy();
			}
		},
		
		
		
		
		
		
		
		
		
		
		
		loadCombobox: function(){
			var oModel = new JSONModel();
			var aTemp = [];
			var oUbicacion1 = [];		
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oUbicaciones = oDimenJSONData.value[i];				
				if(oUbicaciones.U_SEI_NIVEL1 && jQuery.inArray(oUbicaciones.U_SEI_NIVEL1, aTemp) < 0){
					aTemp.push(oUbicaciones.U_SEI_NIVEL1);
					oUbicacion1.push({"Value": oUbicaciones.U_SEI_COLEGIO,"Name": oUbicaciones.U_SEI_NIVEL1});
				}
			}
			
			//ESTRUCTURA FISICA
			var oEstruFisica = null;
			getAsync("estructuraFisica");
			oEstruFisica = JSON.parse(sessionStorage.getItem("estructuraFisica"));
			oItems.PhisicalStructure = oEstruFisica.value;
			
			//provisorio
			oItems.Ubicacion1 = oUbicacion1;
			oModel.setData(oItems);			
			return oModel;
		},
		
		validaCambioUbicacion: function(oEvent){
			var oModel = this.getView().getModel();
			var oData = oModel.getData();
			var oValidatedComboBox = oEvent.getSource(),
		    sSelectedKey = oValidatedComboBox.getSelectedKey(),
			sValue = oValidatedComboBox.getValue(), oBusinessPartner;
			
			oDatosUsuario.socio_cod = sSelectedKey;
			oDatosUsuario.cole_cod = sSelectedKey;
			getAsync("sociosNegocio");
			oBusinessPartner = JSON.parse(sessionStorage.getItem("sociosNegocio"));
			
			var oEstruFisica = null;
			getAsync("estructuraFisica");
			oEstruFisica = JSON.parse(sessionStorage.getItem("estructuraFisica"));
			oItems.PhisicalStructure = oEstruFisica.value;
			
			this.getView().byId("inpCustmrName").setValue(oBusinessPartner.CardCode +";"+oBusinessPartner.CardName);
			
		},

		/*
		 * Elimina celdas seleccionadas de la tabla
		 */
		handleEliminBtn: function(oEvent) {
			var oTable = this.getView().byId("idItemsTable");
		    var model = oTable.getModel();
		    var selRowCount = oTable.getSelectedItems().length;
		    if(selRowCount > 0){
			    for (var i = 0; i < selRowCount; i++) {
			        var rowNum = oTable.getSelectedItems()[i].getBindingContext().getPath().replace("/newItems/", "");
					var oData = model.getData();
					oData.newItems.splice(rowNum, 1);
					model.setData(oData);
					model.refresh();
			    }
			} else {
				new MessageBox.show("No se ha seleccionado lineas a eliminar", {
					icon: MessageBox.Icon.WARNING,
					title: "Informacion Crear Llamada se Servicio",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		handleTableSelectDialog: function(oEvent) {
			this.mensaje= null;
			this.validaUbicaciones();
			if(!this.mensaje){
				if (!this._oDialog) {
					this._oDialog = sap.ui.xmlfragment("SeidorB1UI5.view.ItemsMaster", this);
					this._oDialog.setModel(this.oJSONModel);
				}			
				this.getView().addDependent(this._oDialog);		
				this._oDialog.open();
			} else {
				new sap.m.MessageBox.show(this.mensaje, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion Solicitud De Orden",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		handleUnidad: function(oEvent) {
			var items = this.getView().byId("idItemsTable").getItems();
			var oModel = this.getView().byId("idItemsTable").getModel();
			oPurchaseJSONData = oModel.getData();
			
			for(var item = 0; item < items.length; item++){
				var linea = items[item].getCells()[0].getText();
				this.unidad = (items[item].getCells()[6].getText() ? items[item].getCells()[6].getText() : items[items.length - 1].getCells()[6].getText())
				this.unidad = items[item].getCells()[6].getText();
				
				var aTemp = [];
				var aDimension2 = [];
				for(var i = 0; i < oDimenJSONData.value.length; i++){
					var oDimensiones = oDimenJSONData.value[i];				
					//if(oDimensiones.U_SEI_NIVEL1 === this.unidad && 
					if(oDimensiones.U_SEI_NIVEL1 === this.unidad && oDimensiones.U_SEI_NIVEL2 && jQuery.inArray(oDimensiones.U_SEI_NIVEL2, aTemp) < 0){
						aTemp.push(oDimensiones.U_SEI_NIVEL2);
						aDimension2.push({"Name": oDimensiones.U_SEI_NIVEL2});
						this.cardCode = oDimensiones.U_SEI_COLEGIO;
					}
				}
//				if (linea){
//					oPurchaseJSONData.value[item].DocumentLines[item].Dimension2 = aDimension2;
//					this.cargaCbxUnidad(items[item].getCells()[9].getValue(), items[item].getCells()[10].getValue(), item)
//				} else if (linea === ""){
				//oItems.newItems[items.length -1].DocumentLines[item].Dimension2 = aDimension2;
				oItems.newItems[items.length -1].Dimension2 = aDimension2;
//				}
				oModel.setData(oItems);
				oModel.refresh();
			}
		},
		
		handleLoadItems: function(oControlEvent, idCbx) {
			//var selKey = oControlEvent.getParameter("selectedItem").getKey();
			var selText = oControlEvent.getParameter("selectedItem").getText();
			var path = oControlEvent.getSource().getBindingContext().sPath;
			path = path.substr(path.length-1);
			var nivel1 = oItems.newItems[path].Dimension1;
			
			var oModel = new JSONModel();
			var aTemp = [];
			var aDimension3 = [];
			var aDimension4 = [];			
			
			if(idCbx === "dimension2") {
				this._dimension2 = selText;
				for(var i = 0; i < oDimenJSONData.value.length; i++){
					var oDimensiones = oDimenJSONData.value[i];				
					if(oDimensiones.U_SEI_NIVEL1 == nivel1 && 
					   oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
					   oDimensiones.U_SEI_NIVEL3 && 
					   jQuery.inArray(oDimensiones.U_SEI_NIVEL3, aTemp) < 0){						
						aTemp.push(oDimensiones.U_SEI_NIVEL3);
						aDimension3.push({"Name": oDimensiones.U_SEI_NIVEL3});						
					} else if(oDimensiones.U_SEI_NIVEL1 == nivel1 && oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
							   !oDimensiones.U_SEI_NIVEL3 && 
							   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
						aTemp.push(oDimensiones.U_SEI_NIVEL4);
						aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
					}
				}				
				oItems.newItems[path].Dimension3 = aDimension3;
				
				if(aDimension4)
					oItems.newItems[path].Dimension4 = aDimension4;
				
			} else if(idCbx === "dimension3") {
				this._dimension3 = selText;
				for(var i = 0; i < oDimenJSONData.value.length; i++){
					var oDimensiones = oDimenJSONData.value[i];				
					if(oDimensiones.U_SEI_NIVEL1 == nivel1 && 
					   oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
					   oDimensiones.U_SEI_NIVEL3 === this._dimension3 && 
					   oDimensiones.U_SEI_NIVEL4 && 
					   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
						
						aTemp.push(oDimensiones.U_SEI_NIVEL4);
						aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
					}
				}
				oItems.newItems[path].Dimension4 = aDimension4;				
			}
			oModel.setData(oItems);
			oModel.refresh();
//			this.oSelectD3.setModel(oModel);
//			this.getView.getModel().setData(oModel);
		},
		
		/*
		 * 
		 * COMPORTAMIENTO DEL POPUP CON ITEMS
		 * 
		 */
		handleSelect: function(oEvent) {			
			var aContexts = oEvent.getParameter("selectedContexts");			
			// Si se selecciono un items entra en esta validacion y la agrega a la tabla
			if(aContexts){
				var itemCode,itemName,itemsGroupCode,virtualAssetItem,inventoryItem,Properties1;
				
				if (aContexts && aContexts.length){
					aContexts.map(function(oContext) { 
						itemCode 			= oContext.getObject().ItemCode;
						itemName 			= oContext.getObject().ItemName;
						itemsGroupCode 		= oContext.getObject().ItemsGroupCode;
						virtualAssetItem 	= oContext.getObject().VirtualAssetItem;
						inventoryItem		= oContext.getObject().InventoryItem;
						Properties1			= oContext.getObject().Properties1;
					});
				}
				this.cierraFragment();
				var date = new Date();
				var fechaActual = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();			
				this.fecha2 = (this.fecha1 !== null ? this.fecha1 : fechaActual);
				
				var items = this.getView().byId("idItemsTable").getItems();
				//Actualiza la cantidad en el jsondata
				for(var i = 0; i < items.length; i++){
					for(var x = 0; x < this.oItems.newItem.length; x++ ){
						if(items[i].getCells()[1].getText === this.oItems.newItem[x].itemCode &&
							items[i].getCells()[2].getText() === this.oItems.newItem[x].itemName &&
							items[i].getCells()[3].getText() === this.oItems.newItem[x].FreeText &&
							items[i].getCells()[4].getValue() === this.oItems.newItem[x].Quantity &&
							items[i].getCells()[5].getValue() === this.oItems.newItem[x].Isbn &&
							items[i].getCells()[6].getValue() === this.oItems.newItem[x].Dimension2 &&
							items[i].getCells()[7].getValue() === this.oItems.newItem[x].Dimension3 &&
							items[i].getCells()[8].getValue() === this.oItems.newItem[x].Dimension4 ){

								this.oItems.newItem[x].FreeText 	= items[i].getCells()[3].getValue();
								this.oItems.newItem[x].Quantity 	= items[i].getCells()[4].getValue();
								this.oItems.newItem[x].Isbn 		= items[i].getCells()[5].getValue();
								this.oItems.newItem[x].Dimension2 	= items[i].getCells()[6].getValue();
								this.oItems.newItem[x].Dimension3 	= items[i].getCells()[7].getValue();
								this.oItems.newItem[x].Dimension4 	= items[i].getCells()[8].getValue();
						}
					}
				}
				
				if(this.oItems === null){
					this.oItems = {newItem: [{ "VirtualAssetItem": virtualAssetItem,"itemCode": itemCode, "itemName": itemName, "FreeText": "", "Quantity": 1, "Isbn": "", "Dimension1": this.byId("cbxUbicacion").getSelectedItem().getText(),"Dimension2": "", "Dimension3": "", "Dimension4": "", "InventoryItem": inventoryItem, "ItemsGroupCode":itemsGroupCode, "Properties1": Properties1, "RequieredDate" : this.fecha2, "CostingCode" : "D1001"}]};
				} else {
					this.oItems.newItem.push({ "VirtualAssetItem": virtualAssetItem,"itemCode": itemCode, "itemName": itemName, "FreeText": "","Quantity": 1, "Isbn": "", "Dimension1": this.byId("cbxUbicacion").getSelectedItem().getText(), "Dimension2": "", "Dimension3": "", "Dimension4": "", "InventoryItem": inventoryItem, "ItemsGroupCode":itemsGroupCode, "Properties1": Properties1, "RequieredDate" : this.fecha2, "CostingCode" : "D1001"});
				}
				
				//Actualiza el oModel para posterior carga
				var oView = oEvent.getSource();
				var oData = oView.getModel().getData();
				oData.newItems = this.oItems.newItem;
				oView.getModel().setData(oData);
				
				//Recarga la lista con nuevos productos
				this.oTable = this.getView().byId("idItemsTable");
				this.oColumn = this.getView().byId("idColList");
				this.oTable.setModel(this.oJSONModel);
				this.oTable.bindItems("/newItems", this.oColumn);
				
				items = this.getView().byId("idItemsTable").getItems();
				if(itemsGroupCode === 117)
					items[items.length-1].getCells()[5].setEnabled(true);
				
				if(Properties1 === "tYES"){
					items[items.length-1].getCells()[7].setEnabled(true);
					items[items.length-1].getCells()[8].setEnabled(true);
					items[items.length-1].getCells()[9].setEnabled(true);
				}
				this.habilitaCampos();
				this.handleUnidad();
			}
		},
		
		cierraFragment: function(oEvent) {
	        this._oDialog.destroy();  //Second: destoy fragment 
	        this._oDialog=null;  // Third: null name/pointer 
	    },
		
		_filter : function (oEvent) {
			var oFilter = null;

			if (this._oGlobalFilter && this._oPriceFilter) {
				oFilter = new sap.ui.model.Filter([this._oGlobalFilter, this._oPriceFilter], true);
			} else if (this._oGlobalFilter) {
				oFilter = this._oGlobalFilter;
			} else if (this._oPriceFilter) {
				oFilter = this._oPriceFilter;
			}

			// actualizar la lista de enlace	
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter(oFilter);		
		},

		filterGlobally : function(oEvent) {
			var sQuery = oEvent.getParameter("value");
			this._oGlobalFilter = null;

			if (sQuery) {
				this._oGlobalFilter = new Filter([
					new Filter("ItemName", FilterOperator.Contains, sQuery),
					new Filter("ItemCode", FilterOperator.Contains, sQuery)
				], false);
			}

			this._filter(oEvent);
		},
		
		handleChangeDt: function(oEvent) {
			this.fecha1 = oEvent.getParameter("value");
		},
		
		onDelete: function(items){
			for(var i = 0; i < items.length; i++){			
				var  oModel = this.getView().byId("idItemsTable").getModel();
				var oData = oModel.getData();
				var removed = oData.newItems.splice(0, 1);
				oModel.setData(oData);
				oModel.refresh();
			}
		},
		
		/*
		 * Reestablece los campos a de la cabecera a como estaban inicialmente la pagina
		 */
		limpiarCampos: function(){			
			this.byId("cbxDepartamento").setValue("Selec. Departamento");
			this.byId("cbxTipoFinanciamiento").setValue(JSON.parse(sessionStorage.getItem("tiles")).des_fin);
			this.byId("DP1").setDateValue(new Date());
			
		},
		
		validacionEnvio: function() {
			this.mensaje = null;
			var items = this.getView().byId("idItemsTable").getItems();
			
			if(items.length > 0)
				this.validaUbicaciones();
			else if(items.length === 0)
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No hay Articulos para crear una Solicitud." : "- No hay Articulos para crear una Solicitud.");

			if(this.getView().byId("cbxDepartamento").getValue() === "Selec. Departamento")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Se debe seleccionar Departamento." : "- Se debe seleccionar Departamento.");
			
			if(this.getView().byId("cbxTipoFinanciamiento").getValue() === "Selec. Financiamiento")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Se debe seleccionar Financiamiento." : "- Se debe seleccionar Financiamiento.");
			
			if(this.fecha1 === null)
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Se debe seleccionar fecha de Solicitud." : "- Se debe seleccionar fecha de Solicitud.");
			
			for(var i = 0; i < items.length; i++){
				var linea = i + 1;
				if(!items[i].getCells()[5].getValue() && items[i].getCells()[5].getEnabled() === true)
					this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Se debe ingresar ISBN en la linea " + linea + "." : "- Se debe ingesar ISBN en la linea " + linea + ".");
			}
		},
		
		validaUbicaciones: function(){
			
		},
		
		handleSearch: function(oEvent) {
			
		},

		handleClose: function(oEvent) {
			oEvent.getSource().close();
			//oEvent.getSource().destroy();
		},
		
		habilitaCampos: function(){
			
		},
		
	});
	return ReemplazoCreateController;
});