sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast"
], function(Controller, JSONModel, MessageToast) {
	"use strict";

	var DocumentsAcceptController = Controller.extend("SeidorB1UI5.controller.DocumentsAccept", {
		
		formatter: SeidorB1UI5.controller.Formatter, busy: new sap.m.BusyDialog(), hash: "", res: 0, strGRut:  "",
		
		onInit: function() {
			//GUARDA LA FUNCION ACTUALIZAR PARA FORZAR POSTERIOR CARGA DE PANTALLA
			const onAfterShow = () => this.cargaValores();
			this._afterShowDelegate = { onAfterShow };
			if(oPrevious !== "Login" || oPrevious !== "Page")
				this.getView().addEventDelegate(this._afterShowDelegate);
			
			var oPrevious = "Page"; //History.getInstance().getPreviousHash();
			
			this.cargaValores();
		},
		
		cargaValores: function(){
			var originPath = window.location.origin + window.location.pathname;
			
			if (document.cookie.length === 0){
				sessionStorage.clear();
				localStorage.setItem("hash", originPath + window.location.hash);
				delCookies();
				window.location.replace("");
			} else {
				localStorage.setItem("hash", originPath + window.location.hash);
				
				//Carga los valores del sessionStorage en variable 
				if((!oTilesJSONData || oTilesJSONData === undefined) && sessionStorage.getItem("tiles")){
					oTilesJSONData = JSON.parse(sessionStorage.getItem("tiles")).value;
					oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
				} else if(!sessionStorage.getItem("tiles")){
					this.handleLogout();
				}
				
				if(oDatosUsuario.user_name)
				{
					var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
				}
				else
				{
					this.handleLogout();
				}
				
				this.byId("avatar").setInitials(iAvatar);
				this.byId("avatar").setTooltip("Usuario: " + avatar);

				var oModelTile = new JSONModel(oTilesJSONData);
				this.getView().setModel(oModelTile);
				
				if (this.res === 0 && this.strGRut !== this.getParameterByName('rut')){
					this.AceptacionDocumento();
				}
				
				//this.AceptacionDocumento();
			}
		},
		
		AceptacionDocumento: function(){
			var strRut = this.getParameterByName('rut');
			this.strGRut = strRut;
			this.getCode(strRut);
			var strCode = JSON.parse(sessionStorage.getItem("CodigoEmpleado")).value[0].Code;
			this.updateDocument(strCode);
		},
		
		getCode: function(strRut){
			var resultInfo = {};
			limit = "";filtro = "U_idcodigo eq '" + strRut + "'";
			select = "Code";
			getAsync('CodigoEmpleado');
		},
		
		updateDocument: function(strCode){
			var Empleado = {};
			var date = new Date();
			var fechaActual = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
			
			Empleado = {
				"U_estacon": "Y",
				"U_feccon": fechaActual
			}
			
			this.res = updateEmpleado(Empleado, strCode);
			localStorage.clear();
		},
		
		getParameterByName: function (name) {
		    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		    results = regex.exec(localStorage.getItem("hash"));
		    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		},
		
		handleLogout: function (evt) {
			delCookies();
			localStorage.clear();
			window.location.replace("");
		},
		
		onNavButton: function(oEvent) {
			if (this._oDialog)
				this.cierraFragment();
			
			var sPreviousHash = sap.ui.core.routing.History.getInstance().getPreviousHash();
			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				localStorage.clear();
				window.history.go(-1);
			} else {
				localStorage.clear();
				// There is no history!
				// replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("Page", null, true);
			}
		}
	});
	return DocumentsAcceptController;
});