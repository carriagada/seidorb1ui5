jQuery.sap.require("sap.ui.core.format.DateFormat", "SeidorB1UI5.controller.Formatter");

sap.ui.core.mvc.Controller.extend("SeidorB1UI5.controller.ServiceDetail", {
	
	formatter: SeidorB1UI5.controller.Formatter,
	
	// MODIFICAR //
	// A continuación se muestra el procedimiento de enlace de la vista Detalles para mostrar los datos maestros de elementos en el área del encabezado //
	// MODIFICAR //
	onInit: function() {
    			
    	sap.ui.core.UIComponent.getRouterFor(this).attachRouteMatched(function(oEvent) {
			if (oEvent.getParameter("name") === "ServiceDetail") {
				oDimenJSONData = JSON.parse(sessionStorage.getItem("location"));
				oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
				oEspecialidad = JSON.parse(sessionStorage.getItem("serviceEspecialidades"));
				getAsync("numerosDeSerie");
				oSerialNumber = JSON.parse(sessionStorage.getItem("numerosDeSerie"));
				
				this.mensaje = null;
				var oModel = null;
				var view = this.getView();
				
				this.byId("cbxEspecialidad").setValue().setSelectedKey("");
				this.byId("inpInternalSNSearch").setValue("");
				
				this.byId("cbxEspecialidad").setEnabled(false);
				this.byId("inpInternalSNSearch").setEnabled(false);
				
				if(oDatosUsuario.code_rol !== "1") {
					//OCULTA BOTON SI ES NECESARIO SEGUN ID DE COMPONENTE
					var btnApro = view.byId("btnAprobar");
					var btnRech = view.byId("btnRechazar");
					
					btnApro.setVisible(false);
					btnRech.setVisible(false);
				}
				
				oServiceJSONData = JSON.parse(sessionStorage.getItem("services"));
				
				oModel = new sap.ui.model.json.JSONModel();
				
				oModel.setData(oServiceJSONData);
				var oData = oModel.getData();
				oData.Especialidad = oEspecialidad.value;
				oData.SerialNumbers = oSerialNumber.value;
				
				view.setModel(oModel);
		
				this.byId("cbxEspecialidad").setValue().setSelectedKey("");
				this.byId("inpInternalSNSearch").setValue("");
				
				this.byId("cbxEspecialidad").setEnabled(false);
				this.byId("inpInternalSNSearch").setEnabled(false);
				
//				var oListItem = ordersList.getSelectedItem();
//			    oListItem.setMarkLocked(true);
//			    ordersList.setSelectedItem(false);
//			    
//				var context = new sap.ui.model.Context(view.getModel(), '/value/' + oListItem.getBindingContext().getPath().substr(7));
//				view.setBindingContext(context);
//				this.path = oListItem.getBindingContext().getPath().substr(7);
//				
				if(ordersTable){
					//Para el cambio de domingo
					var oListItem = ordersTable;			    
					var context = new sap.ui.model.Context(view.getModel(), oListItem.getBindingContext().getPath());
					this.path = oListItem.getBindingContext().getPath().substr(7);
					view.setBindingContext(context);
					
					if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
						oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
					} else if(!sessionStorage.getItem("tiles")){
						this.handleLogout();
					}
					
					var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
					this.byId("avatar").setInitials(iAvatar);
					this.byId("avatar").setTooltip("Usuario: " + avatar);
					
					var oItemsTbl = this.getView().byId("idItemsTable");
					var oColListItem = this.getView().byId("idColListItem");
					
					var posicion = oListItem.getBindingContext().getPath().substr(7);
					
					var U_SEI_ESPECIALIDADES = oServiceJSONData.value[posicion].U_SEI_ESPECIALIDADES;
					this.byId("cbxEspecialidad").setValue(U_SEI_ESPECIALIDADES).setSelectedKey(U_SEI_ESPECIALIDADES);
					this.byId("inpInternalSNSearch").setValue(oServiceJSONData.value[posicion].InternalSerialNum);
					
					var location = {};
					//Comienzo Logica para mostrar las ubicaciones guardadas
					
					//primera pasada
					if(oServiceJSONData.value[posicion].U_SEI_UBIC12 && oServiceJSONData.value[posicion].U_SEI_UBIC13 || oServiceJSONData.value[posicion].U_SEI_UBIC14 !== null)
						location = {ServiceLocation: [{"Ubicacion2": oServiceJSONData.value[posicion].U_SEI_UBIC12, "Ubicacion3": oServiceJSONData.value[posicion].U_SEI_UBIC13, "Ubicacion4": oServiceJSONData.value[posicion].U_SEI_UBIC14}]};
					//segunda pasada y mas
					if(oServiceJSONData.value[posicion].U_SEI_UBIC22 && oServiceJSONData.value[posicion].U_SEI_UBIC23 || oServiceJSONData.value[posicion].U_SEI_UBIC24 !== null)
						location.ServiceLocation.push({"Ubicacion2": oServiceJSONData.value[posicion].U_SEI_UBIC22, "Ubicacion3": oServiceJSONData.value[posicion].U_SEI_UBIC23, "Ubicacion4": oServiceJSONData.value[posicion].U_SEI_UBIC24});
					
					if(oServiceJSONData.value[posicion].U_SEI_UBIC32 && oServiceJSONData.value[posicion].U_SEI_UBIC33 || oServiceJSONData.value[posicion].U_SEI_UBIC34 !== null)
						location.ServiceLocation.push({"Ubicacion2": oServiceJSONData.value[posicion].U_SEI_UBIC32, "Ubicacion3": oServiceJSONData.value[posicion].U_SEI_UBIC33, "Ubicacion4": oServiceJSONData.value[posicion].U_SEI_UBIC34});
					
					if(oServiceJSONData.value[posicion].U_SEI_UBIC42 && oServiceJSONData.value[posicion].U_SEI_UBIC43 || oServiceJSONData.value[posicion].U_SEI_UBIC44 !== null)
						location.ServiceLocation.push({"Ubicacion2": oServiceJSONData.value[posicion].U_SEI_UBIC42, "Ubicacion3": oServiceJSONData.value[posicion].U_SEI_UBIC43, "Ubicacion4": oServiceJSONData.value[posicion].U_SEI_UBIC44});
					
					if(oServiceJSONData.value[posicion].U_SEI_UBIC52 && oServiceJSONData.value[posicion].U_SEI_UBIC53 || oServiceJSONData.value[posicion].U_SEI_UBIC54 !== null)
						location.ServiceLocation.push({"Ubicacion2": oServiceJSONData.value[posicion].U_SEI_UBIC52, "Ubicacion3": oServiceJSONData.value[posicion].U_SEI_UBIC53, "Ubicacion4": oServiceJSONData.value[posicion].U_SEI_UBIC54});
					
					if(oServiceJSONData.value[posicion].U_SEI_UBIC62 && oServiceJSONData.value[posicion].U_SEI_UBIC63 || oServiceJSONData.value[posicion].U_SEI_UBIC64 !== null)
						location.ServiceLocation.push({"Ubicacion2": oServiceJSONData.value[posicion].U_SEI_UBIC62, "Ubicacion3": oServiceJSONData.value[posicion].U_SEI_UBIC63, "Ubicacion4": oServiceJSONData.value[posicion].U_SEI_UBIC64});
					
					if(oServiceJSONData.value[posicion].U_SEI_UBIC72 && oServiceJSONData.value[posicion].U_SEI_UBIC73 || oServiceJSONData.value[posicion].U_SEI_UBIC74 !== null)
						location.ServiceLocation.push({"Ubicacion2": oServiceJSONData.value[posicion].U_SEI_UBIC72, "Ubicacion3": oServiceJSONData.value[posicion].U_SEI_UBIC73, "Ubicacion4": oServiceJSONData.value[posicion].U_SEI_UBIC74});
					
					if(oServiceJSONData.value[posicion].U_SEI_UBIC82 && oServiceJSONData.value[posicion].U_SEI_UBIC83 || oServiceJSONData.value[posicion].U_SEI_UBIC84 !== null)
						location.ServiceLocation.push({"Ubicacion2": oServiceJSONData.value[posicion].U_SEI_UBIC82, "Ubicacion3": oServiceJSONData.value[posicion].U_SEI_UBIC83, "Ubicacion4": oServiceJSONData.value[posicion].U_SEI_UBIC84});
					
					if(oServiceJSONData.value[posicion].U_SEI_UBIC82 && oServiceJSONData.value[posicion].U_SEI_UBIC93 || oServiceJSONData.value[posicion].U_SEI_UBIC94 !== null)
						location.ServiceLocation.push({"Ubicacion2": oServiceJSONData.value[posicion].U_SEI_UBIC92, "Ubicacion3": oServiceJSONData.value[posicion].U_SEI_UBIC93, "Ubicacion4": oServiceJSONData.value[posicion].U_SEI_UBIC94});
					
					if(oServiceJSONData.value[posicion].U_SEI_UBIC102 && oServiceJSONData.value[posicion].U_SEI_UBIC103 || oServiceJSONData.value[posicion].U_SEI_UBIC104 !== null)
						location.ServiceLocation.push({"Ubicacion2": oServiceJSONData.value[posicion].U_SEI_UBIC102, "Ubicacion3": oServiceJSONData.value[posicion].U_SEI_UBIC103, "Ubicacion4": oServiceJSONData.value[posicion].U_SEI_UBIC104});
					
					var oView = this.getView();
					var oData = oView.getModel().getData();
					var Activities = {Activities:[]};
					var newActivities = {Activities:[]};
					serviceCallId = null;
					
					sessionStorage.removeItem("activities");
					serviceCallId = oServiceJSONData.value[posicion].ServiceCallID;
					getAsync("activities");
					var DateFormat = sap.ui.core.format.DateFormat;
					
					for(var i = 0; i < JSON.parse(sessionStorage.getItem("activities")).value.length; i++){
						var activities = {};
						var oFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({ style: "medium" });
						var sDate = JSON.parse(sessionStorage.getItem("activities")).value[i].ActivityDate + " " + JSON.parse(sessionStorage.getItem("activities")).value[i].ActivityTime;
						var oDate = oFormat.format(new Date(sDate));
						
						activities.Author 	= JSON.parse(sessionStorage.getItem("activities")).value[i].U_SEI_AUTHOR;
						activities.Date 	= oDate;
						activities.Text 	= JSON.parse(sessionStorage.getItem("activities")).value[i].Details;
						activities.ActivityCode = JSON.parse(sessionStorage.getItem("activities")).value[i].ActivityCode;
						
						Activities.Activities.push(activities);
					}
					
					if(this.byId("txtFechApro").getText() === "" || this.byId("txtFechApro").getText() === null)
						this.byId("txtFechApro").setText("N/A");
					
					oData.Activities = Activities.Activities;
					//
					oData.ServiceLocation = location.ServiceLocation;
					oView.getModel().setData(oData);
					//
					oItemsTbl.setModel(oModel);
					oItemsTbl.bindItems("/ServiceLocation", oColListItem);
					
					//Lista.sort(this.getView().byId("CreationDate"), sap.ui.table.SortOrder.Descending, true);
				} else {
					this.handleNavButton();
				}
		    }
		}, this);
	},
	
	handleLogout: function (evt) {
		sessionStorage.clear();
		delCookies();
		window.location.replace("");
	},
	
	/*
	 * carga de combobox unidad 2 segun la unidad seleccionada en cabecera
	 */
	handleUnidad: function(oEvent) {
		var items = this.getView().byId("idItemsTable").getItems();
		var oModel = this.getView().byId("idItemsTable").getModel();
								
		for(var item = 0; item < items.length; item++){
			//var linea = items[item].getCells()[0].getText();
			this.unidad = this.getView().byId("txtDimension1").getText();
			
			var aTemp = [];
			var aDimension2 = [];		
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oDimensiones = oDimenJSONData.value[i];				
				if(oDimensiones.U_SEI_NIVEL1 === this.unidad && 
				   oDimensiones.U_SEI_NIVEL2 && jQuery.inArray(oDimensiones.U_SEI_NIVEL2, aTemp) < 0){
					aTemp.push(oDimensiones.U_SEI_NIVEL2);
					aDimension2.push({"Name": oDimensiones.U_SEI_NIVEL2});
					this.cardCode = oDimensiones.U_SEI_COLEGIO;
				}
			}
			oServiceJSONData.ServiceLocation[item].Dimension2 = aDimension2;
			oModel.setData(oServiceJSONData);
			oModel.refresh();
		}
	},
	
	handleEspecialidad: function(oEvent){
		var selEsp = oEvent.getParameter("selectedItem").getKey();
		
		this.byId("inpInternalSNSearch").setEnabled(selEsp.indexOf("SINFO") >= 0 ? true : false);
	},
	
	handleLoadItems: function(oControlEvent, idCbx) {
		//var selKey = oControlEvent.getParameter("selectedItem").getKey();
		var selText = oControlEvent.getParameter("selectedItem").getText();
		var linea = oControlEvent.getSource().getBindingContext().getObject().LineNum;
		var cadena = oControlEvent.getSource().getBindingContext().sPath;
		var largo = cadena.length;
		cadena = cadena.substring(largo, largo-2);
		
		if(linea === undefined)
			linea = cadena.replace("/", "");
				
		var oModel = new sap.ui.model.json.JSONModel();
		var aTemp = [];
		var aDimension3 = [];
		var aDimension4 = [];			
		
		if(idCbx === "dimension2") {
			var items = this.getView().byId("idItemsTable").getItems();
			
			items[linea].getCells()[1].setValue("");
			items[linea].getCells()[2].setValue("");
			
			this._dimension2 = selText;			
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oDimensiones = oDimenJSONData.value[i];				
				if(oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
				   oDimensiones.U_SEI_NIVEL3 && 
				   jQuery.inArray(oDimensiones.U_SEI_NIVEL3, aTemp) < 0){						
					aTemp.push(oDimensiones.U_SEI_NIVEL3);
					aDimension3.push({"Name": oDimensiones.U_SEI_NIVEL3});						
				} else if(oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
						   !oDimensiones.U_SEI_NIVEL3 && 
						   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
					aTemp.push(oDimensiones.U_SEI_NIVEL4);
					aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
				}
			}
			oServiceJSONData.ServiceLocation[linea].Dimension3 = aDimension3;
			
			if(aDimension4)
				//provisorio
				oServiceJSONData.ServiceLocation[linea].Dimension4 = aDimension4;
			
		} else if(idCbx === "dimension3") {
			this._dimension3 = selText;
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oDimensiones = oDimenJSONData.value[i];				
				if(oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
				   oDimensiones.U_SEI_NIVEL3 === this._dimension3 && 
				   oDimensiones.U_SEI_NIVEL4 && 
				   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
					
					aTemp.push(oDimensiones.U_SEI_NIVEL4);
					aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
				}
			}
			oServiceJSONData.ServiceLocation[linea].Dimension4 = aDimension4;
		}			
		oModel.setData(oServiceJSONData);
		oModel.refresh();
	},
	
	loadCombobox: function(index){
		if(oServiceJSONData.value[index].U_SEI_UBICACION1 === ""){} 
	},
	
	handleHabilBtn:function(oEvent) {
		sap.ui.core.BusyIndicator.show(0);
		var docStatusTxt = this.getView().byId("txtCabEstado").getText();
		if(docStatusTxt === "Pendiente"){
			this.handleUnidad();
			this.habilitaLineas(true);
			this.byId("cbxEspecialidad").setEnabled(true);
			this.byId("inpInternalSNSearch").setEnabled(this.byId("cbxEspecialidad").getSelectedKey().indexOf("SINFO") >= 0 ? true : false);
			
			this.getView().byId("idItemsTable").setMode("MultiSelect");
			this.habilitaBotones(true);
			sap.ui.core.BusyIndicator.hide();
		} else {
			new sap.m.MessageBox.show("El documento no se puede Modificar porque no se encuentra Pendiente.", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Información",
				onClose: function(oAction){
					sap.ui.core.BusyIndicator.hide();
				}
			});
		}
	},
	
	handleModifiBtn: function(){
		var service = {ServiceLocation:[]};
		var serviceLocation = {};		
		var items = this.getView().byId("idItemsTable").getItems();
		var loRouter = sap.ui.core.UIComponent.getRouterFor(this);
		
		if(this.byId("cbxEspecialidad").getSelectedKey().indexOf("SINFO") >= 0){
			if(this.byId("inpInternalSNSearch").getValue() == ""){
				new sap.m.MessageBox.show("Debe seleccionar un Numero de Serie Interno antes de continuar", {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Informacion Llamada se Servicio",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
				return;
			}
		}
		
		for(var item = 0; item < items.length; item++){
			serviceLocation = {
				U_SEI_NIVEL2:		items[item].getCells()[0].getValue(),
				U_SEI_NIVEL3:		items[item].getCells()[1].getValue(),
				U_SEI_NIVEL4:		items[item].getCells()[2].getValue(),
			}
			service.ServiceLocation.push(serviceLocation);
		}
		
		//this.handleUpdateBtn("");
		
		if(items[items.length-1].getCells()[0].getValue() && items[items.length-1].getCells()[1].getValue() && 
					(oServiceJSONData.ServiceLocation[items.length-1].Ubicacion4.length > 0 && items[items.length-1].getCells()[2].getValue()) ){
				//Llamada al Borrado de SAP
				this.handleUpdateBtn("");
											 					
		} else if(items[items.length-1].getCells()[0].getValue() && items[items.length-1].getCells()[1].getValue() &&
			oServiceJSONData.ServiceLocation[items.length-1].Ubicacion4.length === 0) {
			//Llamada al Borrado de SAP
			this.handleUpdateBtn("");
				   
		} else {
			var  oModel = this.getView().byId("idItemsTable").getModel();
			var oData = oModel.getData();
			new sap.m.MessageBox.show("Debes Llenar la linea anterior antes de Guardar las Modificaciones \n o no se han realizado Modificaciones", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion Llamada se Servicio",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
		    		oModel.setData(oData);
					oModel.refresh();
				}
			});
		}
	},
	
	/*
	 * Funcion encargada de actualiza los campos de la tabla
	 */
	handleUpdateBtn: function(oEvent){
		var items = this.getView().byId("idItemsTable").getItems();
		var location = {}

		location.U_SEI_COD_SUC = oDatosUsuario.code_suc;
		location.U_SEI_ESPECIALIDADES = this.byId("cbxEspecialidad").getSelectedKey();
		
		if(this.byId("cbxEspecialidad").getSelectedKey().indexOf("SINFO") >= 0){
			if(this.byId("inpInternalSNSearch").getValue() !== ""){
				location.InternalSerialNum = this.byId("inpInternalSNSearch").getValue();
				location.ItemCode = this._nom;
			}
		}
		
		location.U_SEI_UBIC12 = items[0] === undefined ? null : items[0].getCells()[0].getValue();
		location.U_SEI_UBIC13 = items[0] === undefined ? null : items[0].getCells()[1].getValue();
		location.U_SEI_UBIC14 = items[0] === undefined ? null : items[0].getCells()[2].getValue();

		location.U_SEI_UBIC22 = items[1] === undefined ? null : items[1].getCells()[0].getValue();
		location.U_SEI_UBIC23 = items[1] === undefined ? null : items[1].getCells()[1].getValue();
		location.U_SEI_UBIC24 = items[1] === undefined ? null : items[1].getCells()[2].getValue();

		location.U_SEI_UBIC32 = items[2] === undefined ? null : items[2].getCells()[0].getValue();
		location.U_SEI_UBIC33 = items[2] === undefined ? null : items[2].getCells()[1].getValue();
		location.U_SEI_UBIC34 = items[2] === undefined ? null : items[2].getCells()[2].getValue();

		location.U_SEI_UBIC42 = items[3] === undefined ? null : items[3].getCells()[0].getValue(); 
		location.U_SEI_UBIC43 = items[3] === undefined ? null : items[3].getCells()[1].getValue();
		location.U_SEI_UBIC44 = items[3] === undefined ? null : items[3].getCells()[2].getValue();

		location.U_SEI_UBIC52 = items[4] === undefined ? null : items[4].getCells()[0].getValue();
		location.U_SEI_UBIC53 = items[4] === undefined ? null : items[4].getCells()[1].getValue();
		location.U_SEI_UBIC54 = items[4] === undefined ? null : items[4].getCells()[2].getValue();
		
		location.U_SEI_UBIC62 = items[5] === undefined ? null : items[5].getCells()[0].getValue();
		location.U_SEI_UBIC63 = items[5] === undefined ? null : items[5].getCells()[1].getValue();
		location.U_SEI_UBIC64 = items[5] === undefined ? null : items[5].getCells()[2].getValue();
		
		location.U_SEI_UBIC72 = items[6] === undefined ? null : items[6].getCells()[0].getValue();
		location.U_SEI_UBIC73 = items[6] === undefined ? null : items[6].getCells()[1].getValue();
		location.U_SEI_UBIC74 = items[6] === undefined ? null : items[6].getCells()[2].getValue();
		
		location.U_SEI_UBIC82 = items[7] === undefined ? null : items[7].getCells()[0].getValue();
		location.U_SEI_UBIC83 = items[7] === undefined ? null : items[7].getCells()[1].getValue();
		location.U_SEI_UBIC84 = items[7] === undefined ? null : items[7].getCells()[2].getValue();
		
		location.U_SEI_UBIC92 = items[8] === undefined ? null : items[8].getCells()[0].getValue();
		location.U_SEI_UBIC93 = items[8] === undefined ? null : items[8].getCells()[1].getValue();
		location.U_SEI_UBIC94 = items[8] === undefined ? null : items[8].getCells()[2].getValue();
		
		location.U_SEI_UBIC102 = items[9] === undefined ? null : items[9].getCells()[0].getValue();
		location.U_SEI_UBIC103 = items[9] === undefined ? null : items[9].getCells()[1].getValue();
		location.U_SEI_UBIC104 = items[9] === undefined ? null : items[9].getCells()[2].getValue();
		
		var docEntry = this.getView().byId("txtnDoc").getText();
		if(!updateService(docEntry, location)){
			this.habilitaBotones(false);
		}
	},
	
	/*
	 * Funcion encargada de Eliminar los items seleccionados 
	 */
	handleEliminBtn: function(oEvent){
		var oTable = this.getView().byId("idItemsTable");
	    var data = oTable.getModel();
	    var selRowCount = oTable.getSelectedItems().length;
	    var docEntry = this.getView().byId("txtnDoc").getText();
	    var rowNum;
	    
	    
	    if(selRowCount > 0){
	    	if(selRowCount < oTable.getItems().length){
	    		var contRev = selRowCount;
			    for (var i = selRowCount; i > 0; i--) {
			    	var cadena = oTable.getSelectedItems()[i-1].getCells()[0].sId;
					var largo = cadena.length;
			    	
			    	rowNum = oTable.getSelectedItems()[i-1].getCells()[0].sId.substring(largo, largo-2).replace("-", "");
					var oData = data.getData();
					oData.ServiceLocation.splice(rowNum, 1);
					data.setData(oData);
					data.refresh();
			    }
			    
			    var service = {ServiceLocation:[]};
				var serviceLocation = {};		
				var items = this.getView().byId("idItemsTable").getItems();
				var loRouter = sap.ui.core.UIComponent.getRouterFor(this);
				
				for(var item = 0; item < items.length; item++){
					serviceLocation = {
						U_SEI_NIVEL2:		items[item].getCells()[0].getValue(),
						U_SEI_NIVEL3:		items[item].getCells()[1].getValue(),
						U_SEI_NIVEL4:		items[item].getCells()[2].getValue(),
					}
					service.ServiceLocation.push(serviceLocation);
				}
				//Llamada al Borrado de SAP
				this.handleUpdateBtn("");
	    	} else {
	    		new sap.m.MessageBox.show("Se Debe Dejar al menos una Linea en las Ubicaciones", {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Informacion Modificar Llamadas de Servicio",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
	    	}
	    }else {
			new sap.m.MessageBox.show("No se ha seleccionado lineas a eliminar", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion Modificar Llamadas de Servicio",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}
	},
	
	// A continuación se muestra la definición de la función para 
	// llamar a la capa de servicio para "Aprobar" un pedido de cliente "Pendiente"
	handleAceptBtn: function(oEvent) {
		sap.ui.core.BusyIndicator.show(0);
//		var oListItem = ordersList.getSelectedItem();
//		oListItem.setMarkLocked(true);
//		ordersList.setSelectedItem(false);
//		
//		var id = oListItem.getBindingContext().getPath().substr(7);
		var oFecha = sap.ui.core.format.DateFormat.getDateTimeInstance({ pattern: "yyyy-MM-dd" });		
		var oDate = new Date();
		var sFech = oFecha.format(oDate);
		
		
		var docEntry = this.getView().byId("txtnDoc").getText();
		var docStatusTxt = this.getView().byId("txtCabEstado");//
		
		if(docStatusTxt.getText() === "Pendiente"){
			docStatusTxt.setText("Aprobado");		
			var body = {"U_SEI_ESTADO": "Aprobado", "U_SEI_FECHAPRO": sFech, "Status": 2};
			
			updateService(docEntry, body);
			new sap.m.MessageBox.show("El documento Cambio de estado a Aprobado", {
				icon: sap.m.MessageBox.Icon.SUCCESS,
				title: "Informacion",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		} else if (docStatusTxt.getText() === "Aprobado"){
			new sap.m.MessageBox.show("El documento ya se encuentra Aprobado", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Información",
				onClose: function(oAction){
					sap.ui.core.BusyIndicator.hide();
				}
			});
		} else {
			new sap.m.MessageBox.show("El documento no se encuentra Pendiente", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}
	},
	
	// A continuación se muestra la definición de la función para 
	// llamar a la capa de servicio para "Rechazar" un pedido de cliente "Pendiente"
	handleCancelBtn: function(oEvent) {
//		var oListItem = ordersList.getSelectedItem();
//		oListItem.setMarkLocked(true);
//		ordersList.setSelectedItem(false);
//		
//		var id = oListItem.getBindingContext().getPath().substr(7);
	    
	    var oFecha = sap.ui.core.format.DateFormat.getDateTimeInstance({ pattern: "yyyy-MM-dd" });		
		var oDate = new Date();
		var sFech = oFecha.format(oDate);
	    
		var docEntry = this.getView().byId("txtnDoc").getText();
		var docStatusTxt = this.getView().byId("txtCabEstado");
		
		if(docStatusTxt.getText() === "Pendiente"){
			docStatusTxt.setText("Rechazada");
			var body = {"U_SEI_ESTADO": "Rechazada", "U_SEI_FECHAPRO": sFech, "Status": -1, "Resolution": "Rechazada en Aplicacion Web"};
			
			var mensaje = updateService(docEntry, body);
			if(mensaje){
				new sap.m.MessageBox.show("Service Layer La modificacion del estado de la Llamada de Servicio a fallado: " + mensaje.message.value, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion",
					onClose: function(oAction) {					
						sap.ui.core.BusyIndicator.hide();
					}
				});
			}else{
				docStatusTxt.setText("Rechazada");
				new sap.m.MessageBox.show("El documento Cambio de estado a Rechazado", {
					icon: sap.m.MessageBox.Icon.SUCCESS,
					title: "Informacion",
					onClose: function(oAction) {					
						sap.ui.core.BusyIndicator.hide();
					}
				});
			}
			
		} else {
			new sap.m.MessageBox.show("El documento no se encuentra Pendiente!", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}		
	},
			
	handleNavButton: function(oEvent) {
		var sPreviousHash = sap.ui.core.routing.History.getInstance().getPreviousHash();
		this.getView().byId("idItemsTable").setMode("None");
		this.habilitaLineas(false);
		this.habilitaBotones(false);
		
		//The history contains a previous entry
		if (sPreviousHash !== undefined) {
			window.history.go(-1);
		} else {
			// There is no history!. replace the current hash with page 1 (will not add an history entry)
			this.getOwnerComponent().getRouter().navTo("ServiceMaster", oServiceJSONData, true);
		}
	},

	validaUbicaciones: function(){
		var items = this.getView().byId("idItemsTable").getItems();
		this.mensaje = null;
		for(var i = 0; i < items.length; i++){			
			if(!items[i].getCells()[0].getValue() || !items[i].getCells()[1].getValue()){
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -Debe seleccionar Ubicaciones. En la posicion " + items.length : "- Debe seleccionar Ubicaciones. En la posicion " + items.length);
			} else if(items[i].getCells()[1].getValue() && oServiceJSONData.ServiceLocation[i].Dimension3 !== undefined){
				if(oServiceJSONData.ServiceLocation[i].Dimension4.length > 0 && !items[i].getCells()[2].getValue()){
					this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -Debe seleccionar Ubicaciones. En la posicion " + items.length : "- Debe seleccionar Ubicaciones. En la posicion " + items.length);
				 }
			}
		}
	},
	
	onExit: function() {
		if (this._oActionSheet) {
			this._oActionSheet.destroy();
			this._oActionSheet = null;
		}
		//this.oProductsModel.destroy();
	},
	
	openActionSheet: function() {
		if (!this._oActionSheet) {
			this._oActionSheet = new sap.m.ActionSheet({
				buttons: new sap.ushell.ui.footerbar.AddBookmarkButton()
			});
			this._oActionSheet.setShowCancelButton(true);
			this._oActionSheet.setPlacement(sap.m.PlacementType.Top);
		}
		
		this._oActionSheet.openBy(this.getView().byId("actionButton"));
	},
	
	getSelectedRowContext: function(sTableId, fnCallback) {
		var oTable = this.byId(sTableId);
		var iSelectedIndex = oTable.getSelectedIndex();

		if (iSelectedIndex === -1) {
			MessageToast.show("Please select a row!");
			return;
		}

		var oSelectedContext = oTable.getContextByIndex(iSelectedIndex);
		if (oSelectedContext && fnCallback) {
			fnCallback.call(this, oSelectedContext, iSelectedIndex, oTable);
		}

		return oSelectedContext;
	},

	onPost: function(oEvent) {
		var DateFormat = sap.ui.core.format.DateFormat;
		var oFormat = DateFormat.getDateTimeInstance({ style: "medium" });
		var oFecha = DateFormat.getDateTimeInstance({ pattern: "yyyy-MM-dd" });
		var oHora = DateFormat.getDateTimeInstance({ pattern: "HH:mm:ss" });
		
		var oDate = new Date();
		var sDate = oFormat.format(oDate);
		var sFech = oFecha.format(oDate);
		var sHora = oHora.format(oDate);
		//creacion de mensaje
		var sValue = oEvent.getParameter("value");
		var  oEntry = {
			    "ActivityDate": sFech,
			    "ActivityTime": sHora,			
				"Author": oDatosUsuario.user_name,
				"Date": "" + sDate,
				"Text":sValue
		};
		
		var oModel = this.getView().getModel();
		var aEntries = oModel.getData().Activities;
		aEntries.unshift(oEntry);
		oModel.refresh();
		this.setActivity(oEntry);
		this.getView().byId("Feed").setEnabled(false);
	},
	
	setActivity: function(oEntry){
		var oActivities = {};
		var _serviceCallId = oServiceJSONData.value[this.path].ServiceCallID
		//var activities = this.getView().getModel().getData().Activities[0];
		oActivities.U_SEI_AUTHOR 		= oEntry.Author;
		oActivities.ActivityDate 		= oEntry.ActivityDate;				
		oActivities.ActivityTime 		= oEntry.ActivityTime;
		oActivities.Details 			= oEntry.Text;
		oActivities.Duration 			= 900;
		oActivities.DurationType 		= "du_Minuts";
		oActivities.Reminder 			= "tYES";
		oActivities.ReminderPeriod 		= 15;
		oActivities.ReminderType 		= "du_Minuts";
		oActivities.Notes 				= "";
		oActivities.ParentObjectId 		= _serviceCallId//N° de llamada de servicio(se actualiza cuando se haya creado la llamada)
		oActivities.ParentObjectType 	= "191"
		
		var res = setActivities(oActivities);
		sap.ui.core.BusyIndicator.hide();
		
		this.updateServices(_serviceCallId, res);
	},
	
	updateServices: function(_serviceCallId, activityCode) {
		var serviceCall = {};
		serviceCall.ServiceCallActivities = [{"ActivityCode": activityCode}]
		
		var servicesCallActivities = oServiceJSONData.value[this.path].ServiceCallActivities
		for(var i = 0; i < servicesCallActivities.length; i++){
			serviceCall.ServiceCallActivities.push({ "LineNum":servicesCallActivities[i].LineNum, "ActivityCode": servicesCallActivities[i].ActivityCode})
		}
		addActivitiesService(_serviceCallId, serviceCall);	
	},

	habilitaBotones: function(estado){
		var btnUb = this.getView().byId("btnUbica");
		var btnUp = this.getView().byId("btnUpdate");
		var btnEl = this.getView().byId("btnElimin");
		btnUb.setVisible(estado);
		btnUp.setEnabled(estado);
		btnEl.setEnabled(estado);
	},
	
	habilitaLineas: function(estado){
		var items = this.getView().byId("idItemsTable").getItems();
		
		for(var item = 0; item < items.length; item++){
			for(var cell = 0; cell < items[item].getCells().length; cell++){
				items[item].getCells()[cell].setEnabled(estado);
			}
		}
	},
	
	onValueHelpRequestedNS: function() {
		this._oInput = this.getView().byId("inpInternalSNSearch");
		this.oColModel = new sap.ui.model.json.JSONModel();
		this.oColModel.setData(
				{
					"cols": [
						{ 
							"label": "Numero de Serie Interno", 
							"template": "InternalSerialNum", 
							"width": "10rem" 
						}, 
						{ 
							"label": "Codigo Producto", 
							"template": "ItemCode" 
						},
						{ 
							"label": "Descripcion Producto", 
							"template": "ItemDescription" 
						}
					]
				}
		);
		
		var aCols = this.oColModel.getData().cols;
		this._oBasicSearchField = new sap.m.SearchField({showSearchButton: false});
		
		this._oValueHelpDialog = sap.ui.xmlfragment("SeidorB1UI5.view.fragment.NumerosDeSerie", this);
		this.getView().addDependent(this._oValueHelpDialog);

		this._oValueHelpDialog.getFilterBar().setBasicSearch(this._oBasicSearchField);
		
		this._oValueHelpDialog.getTableAsync().then(function (oTable) {
			oTable.setModel(this.getView().getModel());
			oTable.setModel(this.oColModel, "columns");

			if (oTable.bindRows) {
				oTable.bindAggregation("rows", "/SerialNumbers");
			}

			if (oTable.bindItems) {
				oTable.bindAggregation("items", "/SerialNumbers", function () {
					return new sap.m.ColumnListItem({
						cells: aCols.map(function (column) {
							return new sap.m.Label({ text: "{" + column.template + "}" });
						})
					});
				});
			}

			this._oValueHelpDialog.update();
		}.bind(this));

		var oToken = new sap.m.Token();
		oToken.setKey(this._oInput.getSelectedKey());
		oToken.setText(this._oInput.getValue());
		this._oValueHelpDialog.setTokens([oToken]);
		this._oValueHelpDialog.open();
	},
	
	onFilterBarSearchNS: function (oEvent) {
		var sSearchQuery = this._oBasicSearchField.getValue(),
			aSelectionSet = oEvent.getParameter("selectionSet");
		var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
			if (oControl.getValue()) {
				aResult.push(new sap.ui.model.Filter({
					path: oControl.getName(),
					operator: sap.ui.model.FilterOperator.Contains,
					value1: oControl.getValue()
				}));
			}

			return aResult;
		}, []);

		aFilters.push(new sap.ui.model.Filter({
			filters: [
				new sap.ui.model.Filter({ path: "InternalSerialNum", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery }),
				new sap.ui.model.Filter({ path: "ItemDescription", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery })
			],
			and: false
		}));

		this._filterTable(new sap.ui.model.Filter({
			filters: aFilters,
			and: true
		}));
	},
	
	_filterTable: function (oFilter) {
		var oValueHelpDialog = this._oValueHelpDialog;

		oValueHelpDialog.getTableAsync().then(function (oTable) {
			if (oTable.bindRows) {
				oTable.getBinding("rows").filter(oFilter);
			}

			if (oTable.bindItems) {
				oTable.getBinding("items").filter(oFilter);
			}

			oValueHelpDialog.update();
		});
	},
	
	onValueHelpOkPressNS: function (oEvent) {
		sap.ui.core.BusyIndicator.show(0);
		
		var aTokens = oEvent.getParameter("tokens");
		//this._oInput.setValue(aTokens[0].getKey());
		var nom = aTokens[0].getText();
		nom = nom.replace(" ("+ aTokens[0].getKey() +")", "");
		this.byId("inpInternalSNSearch").setValue(aTokens[0].getKey());
		this._nom = nom;
		
		sap.ui.core.BusyIndicator.hide();
		
		this._oValueHelpDialog.close();
		
	},
	
	onValueHelpCancelPress: function () {
		this._oValueHelpDialog.close();
	},

	onValueHelpAfterClose: function () {
		this._oValueHelpDialog.destroy();
	},
	
	/*
	 * Evento del Boton "Nueva Ubicacion" el que agrega nuannueva linea 
	 * para agregar nuevas ubicaciones
	 */
	handleAgregarService: function(oEvent) {
		//Tomo los item de la tabla si es que lo tiene
		var items = this.getView().byId("idItemsTable").getItems();	
		//si los items son menores a 10 crea una nueva linea
		if(items.length < 10){
			this.validaUbicaciones();
			this.handleUnidad();
			if(this.mensaje === null) {
				//Actualiza los datos de la tabla al jsondata(modelo)
				for(var i = 0; i < items.length; i++){				
					for(var x = 0; x < oServiceJSONData.ServiceLocation.length; x++ ){
						if(items[i].getCells()[0].getValue() === oServiceJSONData.ServiceLocation[x].Ubicacion2 && 
								items[i].getCells()[1].getValue() === oServiceJSONData.ServiceLocation[x].Ubicacion3 &&
								items[i].getCells()[2].getValue() === oServiceJSONData.ServiceLocation[x].Ubicacion4){
							oServiceJSONData.ServiceLocation[x].Ubicacion2 = items[i].getCells()[0].getValue();
							oServiceJSONData.ServiceLocation[x].Ubicacion3 = items[i].getCells()[1].getValue();
							oServiceJSONData.ServiceLocation[x].Ubicacion4 = items[i].getCells()[2].getValue();
						}
					}
				}			
				
				if(oServiceJSONData.ServiceLocation[items.length-1].Dimension3 === undefined && oServiceJSONData.ServiceLocation[items.length-1].Dimension4 === undefined){
					oServiceJSONData.ServiceLocation.push({ "Ubicacion2": "","Ubicacion3": "","Ubicacion4": "" });
				} else if(oServiceJSONData.ServiceLocation[items.length-1].Dimension4 !== undefined) {
					if(items[items.length-1].getCells()[0].getValue() && items[items.length-1].getCells()[1].getValue() && 
							(oServiceJSONData.ServiceLocation[items.length-1].Dimension4.length > 0 && items[items.length-1].getCells()[2].getValue()) ){
								oServiceJSONData.ServiceLocation.push({ "Ubicacion2": "","Ubicacion3": "","Ubicacion4": "" });
								
					   } else if(items[items.length-1].getCells()[0].getValue() && items[items.length-1].getCells()[1].getValue() &&
								oServiceJSONData.ServiceLocation[items.length-1].Dimension4.length === 0) {
							oServiceJSONData.ServiceLocation.push({ "Ubicacion2": "","Ubicacion3": "","Ubicacion4": "" });
					}
				} else {
					var  oModel = this.getView().byId("idItemsTable").getModel();
					var oData = oModel.getData();
					new sap.m.MessageBox.show("Debes Llenar la linea anterior antes de agregar una nueva ubicacion", {
						icon: sap.m.MessageBox.Icon.WARNING,
						title: "Informacion Llamada se Servicio",
						onClose: function(oAction) {					
				    		sap.ui.core.BusyIndicator.hide();
				    		oModel.setData(oData);
							oModel.refresh();
						}
					});
				}
				//Actualiza el oModel para posterior carga
				var oView = oEvent.getSource();
				var oModel = oView.getModel();
				var oData = oView.getModel().getData();
				//oData.newItems = this.oItems.newItem;
				oData.ServiceLocation = oServiceJSONData.ServiceLocation;
				oView.getModel().setData(oData);
				this.handleUnidad();
				//Recarga la lista con nuevos productos
				this.oTable = this.getView().byId("idItemsTable");
				this.oColumn = this.getView().byId("idColListItem");
				this.oTable.setModel(oModel);
				this.oTable.bindItems("/ServiceLocation", this.oColumn);
				this.habilitaLineas(true);
				
				sap.ui.getCore().setModel(new sap.ui.model.json.JSONModel(this.oTable));
			} else {
				new sap.m.MessageBox.show(this.mensaje, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion Llamada se Servicio",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		} else {
			new sap.m.MessageBox.show("Solo se Pueden Agregar Hasta 10 Ubicaciones por Llamadas", {
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Informacion Llamada se Servicio",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}	
	}
});