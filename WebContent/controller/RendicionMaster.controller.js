sap.ui.define([
	"sap/ui/core/mvc/Controller", 
	"sap/ui/core/routing/History",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter",
	"sap/ui/model/Sorter",
	"./Formatter"
], function(Controller, History, FilterOperator, Filter, Sorter, Formatter){
	"use strict";
	var RendicionMasterController = Controller.extend("SeidorB1UI5.controller.RendicionMaster", {
		
		formatter: Formatter,
		
		onInit: function() {
			var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
			
			if(!cookieJSESSIONID) {
				this.handleLogout();
			} else {
				//GUARDA ELA FUNCION ACTUALIZAR PARA FORZAR POSTERIOR CARGA DE PANTALLA
				var oPrevious = History.getInstance().getPreviousHash();
				const onAfterShow = () => this.handleActualizar();
				this._afterShowDelegate = { onAfterShow };
				if(oPrevious !== "RendicionCreate" || oPrevious !== "Page")
					this.getView().addEventDelegate(this._afterShowDelegate);
				
				getAsync('saldoAsignado');
				//valida que el oRendicionJSONData contenga datos 
				//los valores ajax y crea la variable y carga los datos
				if(!oRendicionJSONData || oRendicionJSONData === undefined && sessionStorage.getItem("rendiciones")){
					sap.ui.core.BusyIndicator.show(0);
					
					this.cargaComponentes();
				}
				
				if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
					oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
				} else if(!sessionStorage.getItem("tiles")){
					this.handleLogout();
				}
				
				var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
				this.byId("avatar").setInitials(iAvatar);
				this.byId("avatar").setTooltip("Usuario: " + avatar);
				
				var oTable = this.getView().byId("tablaRendicion");
				var oModelTable = new sap.ui.model.json.JSONModel();
	
				//ordersTable = oTable;
				oModelTable.setData(oRendicionJSONData);
				oTable.setModel(oModelTable);
		    	this.getView().setModel(oModelTable);
		    	
		    	//oTable.sort(this.getView().byId("DocDate"), sap.ui.table.SortOrder.Descending, true);
		    	oTable.sort(this.getView().byId("DocEntry"), sap.ui.table.SortOrder.Descending, true);
	//	    	var rows = oTable.getRows().length + 1;
	//	    	oTable.setVisibleRowCount(rows);
		    	
		    	var fnPress = this.handleActionPress.bind(this);
		    	this.modes=[{
		    		key: "Multi",
					text: "Multiple Actions",
					handler: function(){
						var oTemplate = new sap.ui.table.RowAction({items: [
							new sap.ui.table.RowActionItem({
								type: "Navigation",
								press: fnPress,
								visible: "{Available}"
							})
						]});
						return [2, oTemplate];
					}
		    	}];
		    	this.getView().setModel(new sap.ui.model.json.JSONModel({items: this.modes}), "modes");
				this.switchState("Multi");
			}
		},

		handleLogout: function (evt) {
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},
		
		switchState : function (sKey) {
			var oTable = this.byId("tablaRendicion");
			var iCount = 0;
			var oTemplate = oTable.getRowActionTemplate();
			if (oTemplate) {
				oTemplate.destroy();
				oTemplate = null;
			}
	
			for (var i = 0; i < this.modes.length; i++) {
				if (sKey == this.modes[i].key) {
					var aRes = this.modes[i].handler();
					iCount = aRes[0];
					oTemplate = aRes[1];
					break;
				}
			}
	
			oTable.setRowActionTemplate(oTemplate);
			oTable.setRowActionCount(iCount);
		},
		
		handleActionPress : function(oEvent) {
			var oRow = oEvent.getParameter("row");
			var oItem = oEvent.getParameter("item");
			ordersTable = oItem;
			selectedOrder = oEvent.getParameter("row").getCells()[0].getText();
			sap.ui.core.UIComponent.getRouterFor(this).navTo("RendicionDetail",{from: "rendicionmaster", contextPath: oItem.getBindingContext().getPath().substr(7)});
		},
	
		handleCrearSoliBtnPress: function() {		
			sap.ui.core.UIComponent.getRouterFor(this).navTo("RendicionCreate",{});
		},
		
		handleActualizar: function(){
			oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
			
			this.cargaComponentes();
			
			var oTable = this.getView().byId("tablaRendicion");
		    var data = oTable.getModel();
			var oData = data.getData();
			
			data.setData(oRendicionJSONData);
			data.refresh();
			this.getView().setModel(data);
		},
		
		cargaComponentes: function(){
			getRendiciones();
			//carga los valores del sessionStorage en variable
			oRendicionJSONData = JSON.parse(sessionStorage.getItem("rendiciones"));
			
			oRendicionJSONData.tipoFinanciamientos 	= JSON.parse(sessionStorage.getItem('tipoFinanciamientos')).value;
			oRendicionJSONData.departamentos 		= JSON.parse(sessionStorage.getItem('departamentos')).value;
			oRendicionJSONData.tipoDocumentos 		= JSON.parse(sessionStorage.getItem('tipoDocumentos')).value;
			oRendicionJSONData.rutFuncionarios 		= JSON.parse(sessionStorage.getItem('rutFuncionarios')).value;
			oRendicionJSONData.rutProveedores 		= JSON.parse(sessionStorage.getItem('rutProveedores')).value;
			oRendicionJSONData.tipoRendicion		= JSON.parse(sessionStorage.getItem('tipoRendicion')).value;
			oRendicionJSONData.saldoAsignado		= JSON.parse(sessionStorage.getItem('saldoAsignado')).value;
			oRendicionJSONData.ubicaciones 			= JSON.parse(sessionStorage.getItem('location')).value;
		},
	
		_filter : function () {
			var oFilter = null;
	
			if (this._oGlobalFilter && this._oPriceFilter) {
				oFilter = new sap.ui.model.Filter([this._oGlobalFilter, this._oPriceFilter], true);
			} else if (this._oGlobalFilter) {
				oFilter = this._oGlobalFilter;
			} else if (this._oPriceFilter) {
				oFilter = this._oPriceFilter;
			}
	
			this.byId("tablaRendicion").getBinding("rows").filter(oFilter, "Application");
		},
	
		filterGlobally : function(oEvent) {
			var sQuery = oEvent.getParameter("query");
			this._oGlobalFilter = null;
	
			if (sQuery) {
				this._oGlobalFilter = new Filter([
					new Filter("U_SEI_NFUNCIO", FilterOperator.Contains, sQuery),
					new Filter("U_SEI_FECHAREN", FilterOperator.Contains, sQuery)
				], false);
			}
	
			this._filter();
		},
	
		onBack : function () {
			var sPreviousHash = History.getInstance().getPreviousHash();
	
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getOwnerComponent().getRouter().navTo("Page", null, true);
			}
		}
	});
	return RendicionMasterController;
}, 
/* bExport= */ 
true);