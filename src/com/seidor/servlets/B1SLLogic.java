package com.seidor.servlets;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.net.ssl.*;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.*;
import java.util.List;
import java.util.Properties;

import com.google.gson.*;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.HttpResponse;

import org.apache.log4j.*;
import org.json.JSONObject;

public class B1SLLogic extends HttpServlet {
	
//	ClassLoader classLoader = Thread.currentThread().getContextClassLoader();  
//	InputStream input = classLoader.getResourceAsStream("log4j.properties");
	private Logger log = Logger.getLogger(B1SLLogic.class.getName());
	
	static String B1IF;
	String top;
	String tabla;
	String B1SLAddress;
	String aCmd;
	String actionURI;
	String filter;
	String select;
	String skip;
	String orderby;
	String path;
	String jsonBody; //String body = null;
	String portalVersion;
	static String getAuth;
	
	String jsonMensaje;
	String sessionID;
	String routeID;
	String b1Session;
	boolean estado;
	Properties properties = new Properties();
	InputStream input;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
					throws ServletException, IOException{
		
        BufferedReader bfr = req.getReader();
        jsonBody = getBuffering(bfr);
        input = getServletContext().getResourceAsStream("/WEB-INF/classes/app.properties");
        //Obtiene parametros desde la URL Para Formar la llamada al Service Layer  
        aCmd        = req.getParameter("cmd");
        actionURI   = (req.getParameter("actionUri") == null) ? "" : req.getParameter("actionUri");
        actionURI   = (req.getParameter("actionUri") == null) ? "" : req.getParameter("actionUri");
        
        switch(actionURI) {
			case "SALDOASIGNADO":
				B1SLAddress = "/b1s/v1/sml.svc/";
	            break;
			case "TARJETAEQUIPO":
				B1SLAddress = "/b1s/v1/sml.svc/";
	            break;
			case "REEMP_CABECERA":
				B1SLAddress = "/b1s/v1/sml.svc/";
	            break;
			case "REEMP_HCONTRA":
				B1SLAddress = "/b1s/v1/sml.svc/";
	            break;
			case "REEMP_LICENCI":
				B1SLAddress = "/b1s/v1/sml.svc/";
	            break;
            default:
            	B1SLAddress = "/b1s/v1/";
		        break;
        }

            
           
        
        //if(actionURI.equals("SALDOASIGNADO")) {
        //    B1SLAddress = "/b1s/v1/sml.svc/";
        //} else if (actionURI.equals("TARJETAEQUIPO")){
        //    B1SLAddress = "/b1s/v1/sml.svc/";
        //} else {
        //    B1SLAddress = "/b1s/v1/";
	    //    }
        
        
        //B1SLAddress = (actionURI.equals("SALDOASIGNADO")) ? "/b1s/v1/sml.svc/" : "/b1s/v1/";     
        top 		= (req.getParameter("top") == null) ? "" : req.getParameter("top");
        tabla      = (req.getParameter("tabla") == null) ? "" : req.getParameter("tabla");
        B1IF		= (req.getParameter("B1IF") == null) ? "" : req.getParameter("B1IF");
        filter      = (req.getParameter("filter") == null) ? "" : req.getParameter("filter");
        select 		= (req.getParameter("select") == null) ? "" : req.getParameter("select");
        skip 		= (req.getParameter("skip") == null) ? "" : req.getParameter("skip");
        orderby		= (req.getParameter("orderby") == null) ? "" : req.getParameter("orderby");
        getAuth		= (req.getHeader("Authorization") == null) ? "" : req.getHeader("Authorization");
//        if(actionURI == "SALDOASIGNADO") { B1SLAddress = B1SLAddress + "/sml.svc/"; }
        path        = B1SLAddress + actionURI;
        b1Session 	= (req.getParameter("sessionID") == null) ? "" : req.getParameter("sessionID");
        portalVersion = (req.getParameter("vPortal") == null) ? "" : req.getParameter("vPortal");
        aCmd 		= req.getParameter("cmd");
        jsonMensaje = "";
        estado 		= true;
        
        //Obtiene el SesionID y routeID desde la URL de la llamada POST
        if (req.getParameter("sessionID") == null) {
        	sessionID = "";
        	routeID = "";        	
        } else {
        	sessionID = b1Session.split(";")[0];
        	sessionID = sessionID.split("=")[1];
        	
        	routeID = b1Session.split(";")[1];
        	routeID = routeID.split("=")[1];
        }
        filter = filter.replace("�", "%C3%91");
        
        log.info("#########################################################################");
		log.info("Comienza lectura de de informacion y escritura de logs");
		log.info("B1SLogic cmd: " + aCmd);
		
		switch(aCmd) {
			case "login":
				path = B1SLAddress + "Login";
                callServiceLayer(path, "POST", jsonBody, sessionID, routeID);
                if(!estado)
                	response(sessionID, routeID, jsonMensaje, resp);
                break;
			case "logout":
				callServiceLayer(path, "POST", jsonBody, sessionID, routeID);
				response(sessionID, routeID, jsonMensaje, resp);
				break;
			case "Add":
				callServiceLayer(path, "POST", jsonBody, sessionID, routeID);
				response(sessionID, routeID, jsonMensaje, resp);
				break;
			case "Update":
				callServiceLayer(path, "PATCH", jsonBody, sessionID, routeID);
				response(sessionID, routeID, jsonMensaje, resp);
				break;
			case "DelLine":
				callServiceLayer(path, "DELLINE", jsonBody, sessionID, routeID);
				response(sessionID, routeID, jsonMensaje, resp);
				break;
			case "Get":
				if(B1IF.equals("B1IF")) {
					path = actionURI + top + tabla + select + filter;
					callServiceB1IF(path, "GET", jsonBody, "");
				} else {				
					if(select != "") {
						if(!path.contains("select"))
							path += "?$select=" + select;
					}
					
					if(filter != "") {
						if(select != "") {
							path += "&$filter=" + filter.replace(" ", "%20");
						} else {
							if(!path.contains("filter"))
								path += "?$filter=" + filter.replace(" ", "%20");
						}
					}				
					
					if(skip != "") {
						if(filter != "" || select != "") {
							path += "&$skip=" + skip;
						} else {
							if(!path.contains("skip"))
								path += "?$skip=" + skip;
						}
					}
					
					if(orderby != "") {
						if(filter != "" || select != "" || skip != "") {
							path += "&$orderby=" + orderby.replace(" ", "%20");
						} else {
							if(!path.contains("orderby"))
								path += "?$orderby=" + orderby.replace(" ", "%20");
						}
					}
					
					callServiceLayer(path, "GET", jsonBody, sessionID, routeID);
				}
				response(sessionID, routeID, jsonMensaje, resp);
				break;
			case "Delete":
				callServiceLayer(path, "DEL", jsonBody, sessionID, routeID);
				break;			
			case "Action":
				callServiceLayer(path, "POST", jsonBody, sessionID, routeID);
				break;
			case "AddAttachments":
				//path = actionURI + top + tabla + select + filter;
				//callServiceB1IF(path, "GET", jsonBody, "");
				path = actionURI + "/Upload";
				callServiceB1IF(path, "GET", jsonBody, "");
				
				//callService(path, "POST", jsonBody, "");
				//response("", jsonMensaje, resp);
				break;
		}
	}

	public void callServiceLayer(String path, String method, String jsonBody, String sessionID, String routeID) throws IOException{
		properties.load(input);
				
		//String destination = "https://PF1LGHEF:50000";
		String destination 		= properties.getProperty("destination");
		String vPortal          = properties.getProperty("vPortal");
		HttpsURLConnection conn = null;
		log.info("#########################################################################");
		log.info("Version Portal: " + vPortal);
		log.info("Method: " + method);
		log.info("SessionID: " + sessionID + ", routeID: " + routeID);
		log.info("Body: " + jsonBody);
		log.info("Url: " + destination + path);
		
		try{

			if(vPortal.equals(portalVersion)) {
				//Desabilita Certificado
				Utilidades u = new Utilidades();
				u.disableSSL();
				
				//Para Obtener las Cookies
				CookieManager cookieManager = new CookieManager();
				CookieHandler.setDefault(cookieManager);
				
				URL url = new URL(destination + path);
				conn = (HttpsURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setDoInput(true);
				if(method == "PATCH"){
					method = "POST";
					conn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
					log.info("Cambio Method: " + method);
					log.info("Se agrega a cabecera: X-HTTP-Method-Override: PATCH");
				} else if (method == "DELLINE"){
					method = "POST";
					conn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
					conn.setRequestProperty("B1S-ReplaceCollectionsOnPatch", "true");
				}
				conn.setRequestMethod(method);
				conn.setRequestProperty("Content-Type", "application/json");
				if(aCmd.equals("Get"))
					conn.setRequestProperty("Prefer", "odata.maxpagesize=0");
				
				//Si consulta tiene Cookies se las adjunta
				if (!b1Session.equals(""))
					conn.setRequestProperty("Cookie", b1Session);			
				
				//Consulta si jsonBody viene con datos(contiene body)
				if(!jsonBody.equals("")){
					OutputStream os = conn.getOutputStream();
					os.write(jsonBody.getBytes("UTF-8"));
					os.flush();
					os.close();
				}
				
				InputStream inputStream;
				boolean estError = false;
				//Evalua la conexion
				conn.connect();
				int responseCode = conn.getResponseCode();
				
				if (responseCode < HttpURLConnection.HTTP_BAD_REQUEST){
					inputStream = conn.getInputStream();
				} else {
					inputStream = conn.getErrorStream();
					estError = true;
				}
				BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));			
				//Obtiene Valores despues de a conexion del Login
				if (responseCode < HttpURLConnection.HTTP_BAD_REQUEST && !aCmd.equals("logout")){
					conn.getContent();
				}
				// Obtencion de Cookies de sesion
				List<HttpCookie> cookies = cookieManager.getCookieStore().getCookies();	        
				for (HttpCookie cookie : cookies) {
					if(routeID.equals("") && cookie.getName().equals("ROUTEID")){
						routeID = cookie.getValue();
					} else if (sessionID.equals("") && cookie.getName().equals("B1SESSION")){
						sessionID = cookie.getValue();
					}
				}	        
				
				jsonMensaje = getBuffering(br);
				
				if(estError)
					log.error(jsonMensaje);
				
				if(aCmd.equals("login")){
					if(estado)
						getRoles(sessionID, routeID);
					
					estado = false;
				}
			}else {
				if (jsonMensaje.equals(""))
					jsonMensaje = "{ \"error\": { \"code\": 506, \"message\": { \"value\": \"Version de Portal desactualizada, favor limpiar cookies y cache del navegador y recargue la pagina nuevamente\" }}}";
				//getJsonMessage(JSESSIONID, jsonMensaje);
				estado = false;
			}
			
	        //response(sessionID, routeID, jsonMensaje);
		} catch(Exception ex) {
			//Gson gson = new Gson();			
			//PrintWriter out = resp.getWriter();
			if (jsonMensaje.equals(""))
				jsonMensaje = "{ \"error\": { \"code\": 506, \"message\": { \"value\": \"" + ex.getMessage() + "\" }}}";
			
			estado = false;
			//out.println(gson.toJson(jsonMensaje));
			log.error("Exception Generada: " + ex.getMessage());
			log.error("JSON de Exception Generada: " + jsonMensaje);
		}
	}

	public void callServiceB1IF(String path, String method, String jsonBody, String JSESSIONID) throws IOException{
		properties.load(input);
		String server 			= properties.getProperty("server");
		String port 			= properties.getProperty("port");
		String urlP1			= properties.getProperty("urlP1");
		String receiver_sld_id	= properties.getProperty("receiver_sld_id");
		String urlP2			= properties.getProperty("urlP2");
		
		jsonBody = jsonBody.replace("\"", "");
		jsonBody = jsonBody.replace("�", "%C3%B1");
		
		HttpURLConnection conn = null;
		log.info("#########################################################################");
		log.info("Method: " + method);
		log.info("SessionID: " + JSESSIONID);
		log.info("Body: " + jsonBody);
		log.info("Url: " + server + port + urlP1 + receiver_sld_id + urlP2 + path + "?bpm.pltype=xml");
		
		try{
			//Desabilita Certificado
			Utilidades u = new Utilidades();
			InputStream inputStream;
	        boolean estError = false;
	        
			u.disableSSL();
			

			//Para Obtener las Cookies
			CookieManager cookieManager = new CookieManager();
	        CookieHandler.setDefault(cookieManager);
			
			URL url = new URL(server + port + urlP1 + receiver_sld_id + urlP2 + path + "?bpm.pltype=xml");			
			conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(220 * 1000);
			conn.setReadTimeout(220 * 1000);
			conn.setDoOutput(true);
			conn.setDoInput(true);
//			conn.setUseCaches(false);

			//Armado de cabecera de llamada
			if(method.equals("PATCH")){
				String hMethod = actionURI.contains("Documents/Quotations") ? "updateDocWithSubDeletion" : "PATCH";
				conn.setRequestProperty("X-HTTP-Method-Override", hMethod);
				conn.setRequestProperty("B1S-ReplaceCollectionsOnPatch", "true");
			}
			
			if(getAuth != "") {
				conn.setRequestProperty("Authorization", getAuth);
			}else {
				conn.setRequestProperty("Cookie", "JSESSIONID=" + JSESSIONID);
			}
			
			method = (B1IF.equals("B1IF") || method.equals("PATCH")) ? "POST" : method;
			conn.setRequestMethod(method);
			conn.setRequestProperty("Content-Type", "application/XML");

		 	//Consulta si jsonBody viene con datos(contiene body)
			if(!jsonBody.equals("")){
				OutputStreamWriter os = new OutputStreamWriter(conn.getOutputStream());
				os.write(jsonBody);
		        os.flush();
		        os.close();
			}
			
	        //Evalua la conexion
	        conn.connect();
	        int responseCode = conn.getResponseCode();	        
			if (responseCode < HttpURLConnection.HTTP_BAD_REQUEST){
				inputStream = conn.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
				jsonMensaje = Utilidades.getBuffering(br);
				
				//getJsonMessage(JSESSIONID, jsonMensaje);
			} else {
				switch(responseCode){
					case (301):
						jsonMensaje = "{\"error\": 301, \"value\":\"Movimiento Permanente: el recurso solicitado por el cliente se encuentra ahora en otra ubicacion. Esta nueva ubicacion queda indicada epor la cabecera location.\"}";
						break;
					case 302:
						jsonMensaje = "{\"error\": 302, \"value\":\"Movimiento Temporal: el recurso solicitado por el cliente se encuentra temporalmente en otra ubicacion. El servidor tiene que devolver en la reaspuesta un enlace hippertextual hacia la nueva ubicacion.\"}";
						break;
					case 304:
						jsonMensaje = "{\"error\": 304, \"value\":\"Sin Modificar: El recurso presente en el servidor es identico al que ya esta disponible en el cliente.\"}";
						break;
					case (400):
						jsonMensaje = "{\"error\": 400, \"value\":\"Peticion Incorreca: la sintaxis de la peticionn HTTP es incorrecta.\"}";
						break;
					case 401:
						jsonMensaje = "{\"error\": 401, \"value\":\"No Autorizado: el acceso al recurso solicitado requiere la autenticacion del cliente.\"}";
						break;
					case (403):
						jsonMensaje = "{\"error\": 403, \"value\":\"Acceso Prohibido: el acceso a este recurso esta prohibido.\"}";
						break;
					case (404):
						jsonMensaje = "{\"error\": 404, \"value\":\"No Encontrado: El recuso solicitado no se ha encontrado.\"}";
						break;
					case (405):
						jsonMensaje = "{\"error\": 405, \"value\":\"Metodo no Admitido: el tipo de peticion HTTP utilizado para acceder a este recurso no esta permitido.\"}";
						break;
					case (407):
						jsonMensaje = "{\"error\": 407, \"value\":\"Autenticacion proxy: un servidor proxy envia este tipo de respuesta cuendo requiere la autenticacion del cliente antes de transmitir la peticion a un servidor.\"}";
						break;
					case (500):
						jsonMensaje = "{\"error\": 500, \"value\":\"Error interno del Servidor: Un Problema de Funcionamiento impide al servidor Cursar con exito la peticion.\"}";
						break;
					case (503):
						jsonMensaje = "{\"error\": 503, \"value\":\"Servicio no Disponible: Una sobrecarga del servidor impide tratar la apeticion.\"}";
						break;
					case (505):
						jsonMensaje = "{\"error\": 505, \"value\":\"Version HTTP no compatible: el servidor no puede utilizar la version del protocolo HTTP especificada en la peticion.\"}";
						break;
				}
				log.error("JSON de Exception Generada: " + jsonMensaje);
			}
			
			List<HttpCookie> cookies = cookieManager.getCookieStore().getCookies();
	        // Obtencion de Cookies de sesion        
	        for (HttpCookie cookie : cookies) {
	            if(JSESSIONID.equals("") && cookie.getName().equals("JSESSIONID"))
	            	JSESSIONID = cookie.getValue();
	        }
	        
	        //jsonMensaje = response1.getBody();
	        
		} catch(Exception ex) {
			//Gson gson = new Gson();			
			//PrintWriter out = resp.getWriter();
			//if (jsonMensaje.equals(""))
			jsonMensaje = "{\"error\": 506, \"value\": \"" + ex.toString() + "\" }";
		
			estado = false;
			//out.println(gson.toJson(jsonMensaje));
			log.error("Exception Generada - CallService: " + ex.getMessage());
			log.error("JSON de Exception Generada: " + jsonMensaje);
		}
	}
	
	private void response(String session, String route, String msg, HttpServletResponse resp) throws IOException {
		//Se prepara la cabecera del envio
	    resp.setContentType("application/json");
		resp.addCookie(new Cookie("B1SESSION", sessionID));
		resp.addCookie(new Cookie("ROUTEID", routeID));
		
		Gson gson = new Gson();			
		PrintWriter out = resp.getWriter();
		out.println(gson.toJson(msg));
		//out.println(gson.toJson(JsonMensaje));
	}
	
	private void getRoles(String sesion, String route){
		try{
			if(!jsonBody.equals("")){
				//Obtengo el Username del JSON de Login
				JsonElement eObjBody =  new JsonParser().parse(jsonBody);
				String user = eObjBody.getAsJsonObject().get("UserName").getAsString();
				
				//Se Genera b1Session si contiene sesion y route
				if (sesion.length() != 0)
					b1Session = "B1SESSION=" + sesion + "; ROUTEID=" + route;				
				
				estado = false;
				user = user.replace("�", "%C3%91");
				//URL Obtiene al usuario y su rol segun usuario logeado 
		        //path = "/b1s/v1/sml.svc/USERLOCATIONS?$filter=USER_CODE%20eq%20'" + user + "'";
		        //path = "/b1s/v1/sml.svc/LOGINPORTAL?$filter=USER_CODE%20eq%20'MHETZ'";
		        path = "/b1s/v1/sml.svc/LOGINPORTAL?$filter=USER_CODE%20eq%20'" + user + "'";
				callServiceLayer(path, "GET", "", sesion, route);
								
				//Obtengo valor del rol para usuario logueado
				JsonElement eObjMsg = new JsonParser().parse(jsonMensaje);				
				JsonElement valores = eObjMsg.getAsJsonObject().get("value");
				
				//Validacion si JSON trae datos estos se le asignan a las variables para formar json con informacion y tiles en vista de app
				String user_id = valores.getAsJsonArray().get(0).getAsJsonObject().get("USERID").isJsonNull() ? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("USERID").getAsString();
				String user_code = valores.getAsJsonArray().get(0).getAsJsonObject().get("USER_CODE").isJsonNull() ? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("USER_CODE").getAsString();
				String user_name = valores.getAsJsonArray().get(0).getAsJsonObject().get("U_NAME").isJsonNull() ? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("U_NAME").getAsString();
				String user_email = valores.getAsJsonArray().get(0).getAsJsonObject().get("EMAIL").isJsonNull() ? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("EMAIL").getAsString();
				String code_rol = valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_ROL").isJsonNull() ? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_ROL").getAsString();
				String code_suc = valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_COD_SUC").isJsonNull() ? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_COD_SUC").getAsString();
				String desc_suc = valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_DESC_SUC").isJsonNull()? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_DESC_SUC").getAsString();
				String cole_cod = valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_CARCODE").isJsonNull() ? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_CARCODE").getAsString();
				//String desc_col = valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_CARDNAME").isJsonNull()? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_CARDNAME").getAsString();
				String code_fin = valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_FINANCIA").isJsonNull() ? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_FINANCIA").getAsString();
				String desc_fin = valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_DESC_FIN").isJsonNull() ? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("U_SEI_DESC_FIN").getAsString();
				String pend_pur = valores.getAsJsonArray().get(0).getAsJsonObject().get("CANT_SC").isJsonNull() ? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("CANT_SC").getAsString();
				String pend_ser = valores.getAsJsonArray().get(0).getAsJsonObject().get("CANT_LLSERVI").isJsonNull() ? "" : valores.getAsJsonArray().get(0).getAsJsonObject().get("CANT_LLSERVI").getAsString();
				
				jsonMensaje = "";
				
				jsonMensaje = "{\"code_rol\": \"" + code_rol + "\","
						  + "\"user_id\": \"" + user_id + "\","
						  + "\"user_code\": \"" + user_code + "\","
						  + "\"user_name\": \"" + user_name + "\","
						  + "\"user_email\": \"" + user_email + "\","
						  + "\"cod_suc\": \"" + code_suc + "\","
						  + "\"des_suc\": \"" + desc_suc + "\","
						  + "\"cod_fin\": \"" + code_fin + "\","
						  + "\"des_fin\": \"" + desc_fin + "\","
						  + "\"cole_cod\": \"" + cole_cod + "\","
						  + "\"value\": {";
				if (code_rol.equals("2")) {
					jsonMensaje = jsonMensaje +  "\"TileCollection\": [{"
							  +      "\"icon\": \"sap-icon://customer-order-entry\","
							  +		 "\"number\": \"" + pend_pur + "\","
							  +      "\"type\": \"Monitor\","
							  +      "\"title\": \"Solicitud de Orden\""
							  +   "},{"
							  +      "\"icon\": \"sap-icon://user-settings\","
							  +      "\"number\": \"" + pend_ser + "\","
							  +      "\"title\": \"Llamada de Servicio\""
							  +    "},{"
							  +		 "\"icon\": \"sap-icon://payment-approval\","
							  +      "\"type\": \"Create\","
							  +      "\"title\": \"Rendici�n de Gasto\""
							  +    "},{"
							  +      "\"icon\": \"sap-icon://collections-insight\","
							  +      "\"title\": \"Solicitud de Fondos\""
							  //+	   "},{"
							  //+      "\"icon\": \"sap-icon://company-view\"," 
							  //+      "\"title\": \"Gestion de Reemplazo\"" 
							  +	   "},{" 
							  +      "\"icon\": \"sap-icon://user-edit\","
							  +      "\"title\": \"Gestion de Contrase�a\""
							  +    "}]}}";
				} else if (code_rol.equals("3")) {
					jsonMensaje = jsonMensaje +  "\"TileCollection\": [{"
							  +      "\"icon\": \"sap-icon://customer-order-entry\","
							  +		 "\"number\": \"" + pend_pur + "\","
							  +      "\"type\": \"Monitor\","
							  +      "\"title\": \"Solicitud de Orden\""
							  +   "},{"
							  +      "\"icon\": \"sap-icon://user-settings\","
							  +      "\"number\": \"" + pend_ser + "\","
							  +      "\"title\": \"Llamada de Servicio\""
							  +   "},{"
							  +      "\"icon\": \"sap-icon://create-form\","
							  +      "\"title\": \"Tarjetas de Equipo\""
							  +    "},{" 
							  +      "\"icon\": \"sap-icon://user-edit\","
							  +      "\"title\": \"Gestion de Contrase�a\""
							  +    "}]}}";
				} else if (code_rol.equals("1")) {
					jsonMensaje = jsonMensaje + "\"TileCollection\": [{"
							  +      "\"icon\": \"sap-icon://customer-order-entry\","
							  +		 "\"number\": \"" + pend_pur + "\","
							  +      "\"type\": \"Monitor\","
							  +      "\"title\": \"Solicitud de Orden\""
							  +   "},{"
							  +      "\"icon\": \"sap-icon://create-form\","
							  +      "\"title\": \"Tarjetas de Equipo\""
							  +   "},{"
							  +      "\"icon\": \"sap-icon://user-settings\","
							  +      "\"number\": \"" + pend_ser + "\","
							  +      "\"title\": \"Llamada de Servicio\""
							  +    "},{"
							  +		 "\"icon\": \"sap-icon://payment-approval\","
							  +      "\"type\": \"Create\","
							  +      "\"title\": \"Rendici�n de Gasto\""
							  +    "},{"
							  +      "\"icon\": \"sap-icon://collections-insight\","
							  +      "\"title\": \"Solicitud de Fondos\""
							  +    "},{"
							  +      "\"icon\": \"sap-icon://trip-report\"," 
							  +      "\"title\": \"Reporte Presupuesto\"" 
							  //+	   "},{"
							  //+      "\"icon\": \"sap-icon://company-view\"," 
							  //+      "\"title\": \"Gestion de Reemplazo\"" 
							  +	   "},{" 
							  +      "\"icon\": \"sap-icon://user-edit\","
							  +      "\"title\": \"Gestion de Contrase�a\""
							  +    "}]}}";
				}
				sessionID = sesion;
				routeID = route;				
				//InputStream input = getServletContext().getResourceAsStream("/WEB-INF/classes/app.properties");
				//properties.load(input);
			}
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	private void getJsonMessage(String sesion, String jsonMensaje) {
		//Obtengo valor del rol para usuario logueado
		JsonElement eObjMsg = new JsonParser().parse(jsonMensaje);				
		JsonElement valores = eObjMsg.getAsJsonObject().get("Status");
		JsonElement contador = eObjMsg.getAsJsonObject().get("count");
		
		String DIresult	= valores.getAsJsonObject().get("DIresult").isJsonNull() ? "---" : valores.getAsJsonObject().get("DIresult").getAsString();
		String DImsg 	= valores.getAsJsonObject().get("DImsg").isJsonNull()	 ? "---" : valores.getAsJsonObject().get("DImsg").getAsString();
		String SQLquery = valores.getAsJsonObject().get("SQLquery").isJsonNull() ? "---" : valores.getAsJsonObject().get("SQLquery").getAsString();
		
		log.info("Session: " + sesion + ". Resultado: " + DIresult + ". Mensaje: " + DImsg + ". Retorno : " + contador + " valores.");
		log.info("Query Ejecutada: " + SQLquery);
	}
	
	//CREAR EN CLASES SEPARADAS
	
	public static String getBuffering(BufferedReader bfr){
		String resultado = "";
		try{
			StringBuilder sb = new StringBuilder();
		    String str;
		    while((str = bfr.readLine()) != null ){
		        sb.append(str);
		    } 
		    resultado = new String(sb);
		}catch(Exception ex){
			
		} finally {
			
		}
		return resultado;
	}
	
	private static class MyTrustManager implements X509TrustManager {
		@Override
		public X509Certificate[] getAcceptedIssuers() {
		    return null;
		}
		@Override
		public void checkClientTrusted(X509Certificate[] certs, String authType) 
				throws CertificateException {
		}
		@Override
		public void checkServerTrusted(X509Certificate[] certs, String authType) 
				throws CertificateException {
		}
	}

	private static void disableSSL(){
		try{
			TrustManager[] trustAllCerts = new TrustManager[] { new MyTrustManager() };
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new SecureRandom());
			
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			
		} catch(Exception ex){
			ex.printStackTrace();
		}
	}
}